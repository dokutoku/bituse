/**
 * 第7回 メインループの骨格を完成させよう
 *
 * See_Also:
 *      https://bituse.info/game/7
 */
module bituse_info.game.sample_7.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//キー取得用配列
		char[256] key;

		//x座標
		int x = 300;
		int y = 240;

		//グラフィックハンドル
		int gh;

		//画像読み込み
		gh = dxlib_d.dxlib.LoadGraph("char.png");

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
			dxlib_d.dxlib.DrawGraph(x, y, gh, dxlib_d.dxdatatype.TRUE);

			if (key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) {
				x -= 6;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1) {
				x += 6;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) {
				y -= 6;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1) {
				y += 6;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_ESCAPE] == 1) {
				break;
			}
		}

		// ソフトの終了
		return 0;
	}
