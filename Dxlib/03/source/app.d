/**
 * 第3回 描画の基本
 *
 * See_Also:
 *      https://bituse.info/game/3
 */
module bituse_info.game.sample_3.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		dxlib_d.dxlib.LoadGraphScreen(0, 0, "star.png", dxlib_d.dxdatatype.TRUE);

		//グラフィックハンドル
		int gh;

		gh = dxlib_d.dxlib.LoadGraph("star.png");

		dxlib_d.dxlib.DrawGraph(300, 0, gh, dxlib_d.dxdatatype.TRUE);

		// キー入力待ち
		dxlib_d.dxlib.WaitKey();

		// ソフトの終了
		return 0;
	}
