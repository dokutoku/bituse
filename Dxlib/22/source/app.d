/**
 * 第22回 キャラをジャンプさせてみよう
 *
 * See_Also:
 *      https://bituse.info/game/22
 */
module bituse_info.game.sample_22.app;


version (Windows):

private static import core.stdc.stdlib;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//キー取得用配列
		char[256] key;

		//x座標
		int x = 300;
		int y = 360;
		int y_prev = 0;
		int y_temp = 0;

		//ジャンプしてるかのフラグ。
		bool jflag = false;

		//グラフィックハンドル格納用配列
		int[12] gh;

		//画像読み込み
		dxlib_d.dxlib.LoadDivGraph("charall.png", gh.length, 3, 4, 49, 66, &(gh[0]));

		//移動係数
		float move = 1.0f;

		//横方向と縦方向のカウント数。
		int xcount = 0;
		int ycount = 0;

		//添字用変数
		int ix = 0;
		int iy = 0;
		int result = 0;

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
			if ((key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1)) {
				if ((key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1)) {
					//移動係数を0.71に設定
					move = 0.71f;
				} else {
					//斜めじゃなければ1.0に設定
					move = 1.0f;
				}
			} else if ((key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1)) {
				move = 1.0f;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) {
				x -= cast(int)(4 * move);
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1) {
				x += cast(int)(4 * move);
			}

			//ジャンプ処理

			if (jflag == true) {
				y_temp = y;
				y += (y - y_prev) + 1;
				y_prev = y_temp;

				if (y == 360) {
					jflag = false;
				}
			}

			if ((key[dxlib_d.dxlib.KEY_INPUT_SPACE] == 1) && (jflag == false)) {
				jflag = true;
				y_prev = y;
				y = y - 20;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) {
				if (xcount > 0) {
					xcount = 0;
				}

				--xcount;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1) {
				if (xcount < 0) {
					xcount = 0;
				}

				++xcount;
			}

			//カウント数から添字を求める。
			ix = core.stdc.stdlib.abs(xcount) % 30 / 10;
			iy = core.stdc.stdlib.abs(ycount) % 30 / 10;

			//xカウントがプラスなら右向きなので3行目の先頭添字番号を足す。
			if (xcount > 0) {
				ix += 3;
				result = ix;
			} else if (xcount < 0) {
				//マイナスなら左向きなので、4行目の先頭添字番号を足す。
				ix += 9;
				result = ix;
			}

			dxlib_d.dxlib.DrawGraph(x, y, gh[result], dxlib_d.dxdatatype.TRUE);

			//押されてなければカウントをゼロにする。
			if ((key[dxlib_d.dxlib.KEY_INPUT_LEFT] != 1) && (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] != 1)) {
				xcount = 0;
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_ESCAPE] == 1) {
				break;
			}
		}

		// ソフトの終了
		return 0;
	}
