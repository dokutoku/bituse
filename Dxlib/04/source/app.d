/**
 * 第4回 ゲームプログラミングの仕組み
 *
 * See_Also:
 *      https://bituse.info/game/4
 */
module bituse_info.game.sample_4.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		//ウィンドウモードに設定
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		version (all) {
			while (dxlib_d.dxlib.ProcessMessage() != -1) {
				//～～～ここに処理を書く。
			}
		} else {
			while (true) {
				//～～～～～ここに処理を色々と書く～～～～～～
				////
			}
		}

		// ソフトの終了
		return 0;
	}
