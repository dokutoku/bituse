/**
 * 第2回 DXライブラリのサンプルプログラム
 *
 * See_Also:
 *      https://bituse.info/game/2
 */
module bituse_info.game.sample_2.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		// 点を打つ
		dxlib_d.dxlib.DrawPixel(320, 240, 0xFFFF);

		// 点を打つ
		dxlib_d.dxlib.DrawPixel(340, 240, 0xFFFF);

		// 点を打つ
		dxlib_d.dxlib.DrawPixel(360, 240, 0xFFFF);

		// キー入力待ち
		dxlib_d.dxlib.WaitKey();

		// ソフトの終了
		return 0;
	}

version (none) {
	extern (Windows)
	nothrow @nogc
	int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

		do
		{
			//ウィンドウモード指定。
			dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

			// DXライブラリ初期化処理
			if (dxlib_d.dxlib.DxLib_Init() == -1) {
				// エラーが起きたら直ちに終了
				return -1;
			}

			scope (exit) {
				// DXライブラリ使用の終了処理
				dxlib_d.dxlib.DxLib_End();
			}

			//～～～～ここにやりたい処理を書く～～～～～～～～

			// キー入力待場合は必要
			dxlib_d.dxlib.WaitKey();

			// ソフトの終了
			return 0;
		}
}
