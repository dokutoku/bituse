/**
 * 第29回 単純に音を鳴らす
 *
 * See_Also:
 *      https://bituse.info/game/29
 */
module bituse_info.game.sample_29.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		char[256] key;

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
			if (key[dxlib_d.dxlib.KEY_INPUT_RETURN] == 1) {
				dxlib_d.dxlib.PlaySoundFile("bom.wav", dxlib_d.dxlib.DX_PLAYTYPE_BACK);
			}
		}

		// ソフトの終了
		return 0;
	}
