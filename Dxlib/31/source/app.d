/**
 * 第31回 音声を止める
 *
 * See_Also:
 *      https://bituse.info/game/31
 */
module bituse_info.game.sample_31.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		char[256] key;

		//サウンドハンドル取得。
		int sh = dxlib_d.dxlib.LoadSoundMem("bom.wav");

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
			if (key[dxlib_d.dxlib.KEY_INPUT_RETURN] == 1) {
				dxlib_d.dxlib.PlaySoundMem(sh, dxlib_d.dxlib.DX_PLAYTYPE_BACK, dxlib_d.dxdatatype.TRUE);
			}

			if (key[dxlib_d.dxlib.KEY_INPUT_SPACE] == 1) {
				dxlib_d.dxlib.StopSoundMem(sh);
			}
		}

		// ソフトの終了
		return 0;
	}
