/**
 * 第5回 画像を動かしてみよう
 *
 * See_Also:
 *      https://bituse.info/game/5
 */
module bituse_info.game.sample_5.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//グラフィックハンドル
		int gh;

		//x座標
		int x = 0;

		//画像読み込み
		gh = dxlib_d.dxlib.LoadGraph("char.png");

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		version (all) {
			while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0)) {
				dxlib_d.dxlib.DrawGraph(x, 100, gh, dxlib_d.dxdatatype.TRUE);

				x += 2;

				if (x == 640) {
					break;
				}
			}
		} else {
			while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0)) {
				dxlib_d.dxlib.DrawGraph(x, 100, gh, dxlib_d.dxdatatype.TRUE);

				x += 2;

				if (x == 640) {
					break;
				}
			}
		}

		// キー入力待ち
		dxlib_d.dxlib.WaitKey();

		// ソフトの終了
		return 0;
	}
