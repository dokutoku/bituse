/**
 * 第6回 キーボードのキー押下状態を取得
 *
 * See_Also:
 *      https://bituse.info/game/6
 */
module bituse_info.game.sample_6.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0)) {
			if (dxlib_d.dxlib.CheckHitKey(dxlib_d.dxlib.KEY_INPUT_LEFT) == 1) {
				dxlib_d.dxlib.DrawString(300, 240, "←キーが押されています", 0xFFFF);
			} else if (dxlib_d.dxlib.CheckHitKey(dxlib_d.dxlib.KEY_INPUT_UP) == 1) {
				dxlib_d.dxlib.DrawString(300, 240, "↑キーが押されています", 0xFFFF);
			} else if (dxlib_d.dxlib.CheckHitKey(dxlib_d.dxlib.KEY_INPUT_RIGHT) == 1) {
				dxlib_d.dxlib.DrawString(300, 240, "→キーが押されています", 0xFFFF);
			} else if (dxlib_d.dxlib.CheckHitKey(dxlib_d.dxlib.KEY_INPUT_DOWN) == 1) {
				dxlib_d.dxlib.DrawString(300, 240, "↓キーが押されています", 0xFFFF);
			} else if (dxlib_d.dxlib.CheckHitKey(dxlib_d.dxlib.KEY_INPUT_ESCAPE) == 1) {
				//エスケープキーでループを抜ける。
				break;
			}
		}

		// ソフトの終了
		return 0;
	}
