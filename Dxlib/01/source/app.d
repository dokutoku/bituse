/**
 * 第1回 ゲームを作るには？
 *
 * See_Also:
 *      https://bituse.info/game/1
 */
module bituse_info.game.sample_1.app;


version (Windows):

import core.sys.windows.windows;
import dxlib_d;

extern (Windows)
pure nothrow @safe @nogc @live
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)

	do
	{
		return 0;
	}
