/**
 * 第19回 斜めに移動させる
 *
 * See_Also:
 *      https://bituse.info/game/19
 */
module bituse_info.game.sample_19.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//キー取得用配列
		char[256] key;

		//x座標
		int x = 300;
		int y = 240;

		//グラフィックハンドル
		int gh;

		//画像読み込み
		gh = dxlib_d.dxlib.LoadGraph("char.png");

		//描画先を裏画面に設定
		dxlib_d.dxlib.SetDrawScreen(dxlib_d.dxlib.DX_SCREEN_BACK);

		version (all) {
			//移動係数
			float move = 1.0f;

			while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
				dxlib_d.dxlib.DrawGraph(x, y, gh, dxlib_d.dxdatatype.TRUE);

				if ((key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1)) {
					if ((key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1)) {
						//移動係数を0.71に設定
						move = 0.71f;
					} else {
						//斜めじゃなければ1.0に設定
						move = 1.0f;
					}
				} else if ((key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) || (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1)) {
					move = 1.0f;
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) {
					x -= cast(int)(6 * move);
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1) {
					x += cast(int)(6 * move);
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) {
					y -= cast(int)(6 * move);
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1) {
					y += cast(int)(6 * move);
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_ESCAPE] == 1) {
					break;
				}
			}
		} else {
			while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
				dxlib_d.dxlib.DrawGraph(x, y, gh, dxlib_d.dxdatatype.TRUE);

				if (key[dxlib_d.dxlib.KEY_INPUT_LEFT] == 1) {
					x -= 6;
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_RIGHT] == 1) {
					x += 6;
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_UP] == 1) {
					y -= 6;
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_DOWN] == 1) {
					y += 6;
				}

				if (key[dxlib_d.dxlib.KEY_INPUT_ESCAPE] == 1) {
					break;
				}
			}
		}

		// ソフトの終了
		return 0;
	}
