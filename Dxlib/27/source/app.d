/**
 * 第27回 画像をブレンド(透過)させて描画しよう
 *
 * See_Also:
 *      https://bituse.info/game/27
 */
module bituse_info.game.sample_27.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import dxlib_d.dxdatatype;
private static import dxlib_d.dxfunctionwin;
private static import dxlib_d.dxlib;

/*
 * プログラムは WinMain から始まります
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		dxlib_d.dxfunctionwin.ChangeWindowMode(dxlib_d.dxdatatype.TRUE);

		// DXライブラリ初期化処理
		if (dxlib_d.dxlib.DxLib_Init() == -1) {
			// エラーが起きたら直ちに終了
			return -1;
		}

		scope (exit) {
			// DXライブラリ使用の終了処理
			dxlib_d.dxlib.DxLib_End();
		}

		//キー取得用配列
		char[256] key;

		//x座標
		int x = 300;
		int y = 360;
		int y_prev = 0;
		int y_temp = 0;

		//ジャンプしてるかのフラグ。
		bool jflag = false;

		//グラフィックハンドル
		int gh = dxlib_d.dxlib.LoadGraph("char.png");

		while ((dxlib_d.dxlib.ScreenFlip() == 0) && (dxlib_d.dxlib.ProcessMessage() == 0) && (dxlib_d.dxlib.ClearDrawScreen() == 0) && (dxlib_d.dxlib.GetHitKeyStateAll(&(key[0])) == 0)) {
			//通常描画
			dxlib_d.dxlib.DrawGraph(10, 10, gh, dxlib_d.dxdatatype.TRUE);

			dxlib_d.dxlib.SetDrawBlendMode(dxlib_d.dxlib.DX_BLENDMODE_ALPHA, 122);

			//透過して描画
			dxlib_d.dxlib.DrawGraph(10, 100, gh, dxlib_d.dxdatatype.TRUE);

			//設定を元に戻す。
			dxlib_d.dxlib.SetDrawBlendMode(dxlib_d.dxlib.DX_BLENDMODE_NOBLEND, 0);

			if (key[dxlib_d.dxlib.KEY_INPUT_ESCAPE] == 1) {
				break;
			}
		}

		// ソフトの終了
		return 0;
	}
