/**
 * 第12回
 *
 * See_Also:
 *      https://bituse.info/c/12
 */
module bituse_info.c.sample_12.test1;


import core.stdc.locale;
import core.stdc.stdio;

struct TEST
{
	int figure;
	short value;
	char letter;
}

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		.TEST test;

		//ポインタtestpにtestのアドレスを代入。
		.TEST* testp = &test;

		//アロー演算子ではなく、ドットでアクセスし代入。
		testp.figure = 100;
		testp.value = 10;
		testp.letter = 'A';

		core.stdc.stdio.printf("%d\n", testp.figure);
		core.stdc.stdio.printf("%d\n", testp.value);
		core.stdc.stdio.printf("%c\n", testp.letter);

		return 0;
	}
