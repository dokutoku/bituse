/**
 * 第26回
 *
 * See_Also:
 *      https://bituse.info/c/26
 */
module bituse_info.c.sample_26.test1;


import core.stdc.locale;
import core.stdc.stdio;

//D言語ではこの宣言は必要ない
//void func(int);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//関数ポインタを宣言
		extern (C) nothrow @nogc void function(int) pointer;

		//関数ポインタにfunc関数のアドレスを代入。
		pointer = &.func;

		//関数ポインタから引数に100を渡してfunc関数を実行。
		pointer(100);

		return 0;
	}

extern (C)
nothrow @nogc
void func(int value)

	do
	{
		core.stdc.stdio.printf("%d\n", value);
	}
