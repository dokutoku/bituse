/**
 * 第36回
 *
 * See_Also:
 *      https://bituse.info/c/36
 */
module bituse_info.c.sample_36.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] test = [3, 10, 4, 5, 6, 1, 8, 7, 9, 2];

		//配列の要素数を取得
		size_t number = test.length;

		//先頭より一つ上の要素からチェック
		for (size_t s = 1; s < number; ++s) {
			//ソート済み部分の要素と比較(以降はバブルソート)
			for (size_t i = s; (i != 0) && (test[i] < test[i - 1]); --i) {
				//ソート済みなので自分より小さいところがあればそこへ代入するだけ
				int work = test[i - 1];
				test[i - 1] = test[i];
				test[i] = work;
			}
		}

		//並びかえ出来たか確認
		for (size_t i = 0; i < number; i++) {
			core.stdc.stdio.printf("%d,", test[i]);
		}

		//改行
		core.stdc.stdio.puts("");

		return 0;
	}
