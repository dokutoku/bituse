/**
 * 第10回
 *
 * See_Also:
 *      https://bituse.info/c/10
 */
module bituse_info.c.sample_10.test2;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test = 100;
		int* pointer = &test;

		core.stdc.stdio.printf("%d\n", *pointer);

		return 0;
	}
