/**
 * 第10回
 *
 * See_Also:
 *      https://bituse.info/c/10
 */
module bituse_info.c.sample_10.test3;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test = 100;
		int* pointer = &test;

		//ポインタ変数を使って値を書き換え
		*pointer = 500;

		core.stdc.stdio.printf("%d\n", test);

		return 0;
	}
