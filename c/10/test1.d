/**
 * 第10回
 *
 * See_Also:
 *      https://bituse.info/c/10
 */
module bituse_info.c.sample_10.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//配列bufの先頭アドレスをtestへ代入
		char[50] buf = "あいうえお\0";
		char* test = &(buf[0]);

		core.stdc.stdio.printf("%s", test);

		return 0;
	}
