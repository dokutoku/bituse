/**
 * 第45回
 *
 * See_Also:
 *      https://bituse.info/c/45
 */
module bituse_info.c.sample_45.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

//D言語ではこの宣言は必要ない
//int sum(int*);
//int ave(int*);
//int min(int*);
//int max(int*);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[4] num;
		core.stdc.stdio.printf("値を4つ入力してください(1,2,3,4):");
		core.stdc.stdio.scanf("%d %d %d %d", &num[0], &num[1], &num[2], &num[3]);

		int n;
		core.stdc.stdio.printf("合計数は:%dです。\n", sum(&(num[0])));
		core.stdc.stdio.printf("平均値は:%dです。\n", ave(&(num[0])));
		core.stdc.stdio.printf("最小値は:%dです。\n", min(&(num[0])));
		core.stdc.stdio.printf("最大値は:%dです。\n", max(&(num[0])));

		return 0;
	}

extern (C)
pure nothrow @trusted @nogc @live
int sum(int* all)

	in
	{
		assert(all != null);
	}

	do
	{
		int total = 0;

		for (size_t i = 0; i < 4; i++) {
			total += all[i];
		}

		return total;
	}

extern (C)
pure nothrow @trusted @nogc @live
int ave(int* all)

	in
	{
		assert(all != null);
	}

	do
	{
		int total = 0;

		for (size_t i = 0; i < 4; i++) {
			total += all[i];
		}

		return total / 4;
	}

extern (C)
pure nothrow @trusted @nogc @live
int min(int* all)

	in
	{
		assert(all != null);
	}

	do
	{
		int min = all[0];

		for (size_t i = 1; i < 4; i++) {
			if (all[0] > all[i]) {
				min = all[i];
			}
		}

		return min;
	}

extern (C)
pure nothrow @trusted @nogc @live
int max(int* all)

	in
	{
		assert(all != null);
	}

	do
	{
		int max = all[0];

		for (size_t i = 1; i < 4; i++) {
			if (all[0] < all[i]) {
				max = all[i];
			}
		}

		return max;
	}
