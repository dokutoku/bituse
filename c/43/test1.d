/**
 * 第43回
 *
 * See_Also:
 *      https://bituse.info/c/43
 */
module bituse_info.c.sample_43.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int a;
		int b;
		int c;

		core.stdc.stdio.printf("三角形の3辺の長さ(a,b,c)を入力してください:\n");

		while (true) {
			core.stdc.stdio.printf("a = ");
			core.stdc.stdio.scanf("%d", &a);
			core.stdc.stdio.printf("b = ");
			core.stdc.stdio.scanf("%d", &b);
			core.stdc.stdio.printf("c = ");
			core.stdc.stdio.scanf("%d", &c);

			if (((a + b) >= c) && ((b + c) >= a) && ((c + a) >= b)) {
				core.stdc.stdio.printf("正しい三角形です。\n");

				break;
			} else {
				core.stdc.stdio.printf("正しい三角形ではありません。もう一度入力してください。\n");
			}
		}

		return 0;
	}
