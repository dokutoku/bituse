/**
 * 第31回
 *
 * See_Also:
 *      https://bituse.info/c/31
 */
module bituse_info.c.sample_31.test2;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main(int argc, char** argv)

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//引数が1以上ならパラメータが付加されてる
		if (argc > 1) {
			//引数の数だけループして表示。
			for (int i = 0; i < argc; i++) {
				core.stdc.stdio.printf("%s\n", argv[i]);
			}
		}

		return 0;
	}
