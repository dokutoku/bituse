/**
 * 第31回
 *
 * See_Also:
 *      https://bituse.info/c/31
 */
module bituse_info.c.sample_31.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main(int argc, char** argv)

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.printf("%d\n", argc);

		//最初のアドレスには実行ファイルのパスが入っている
		core.stdc.stdio.printf("%s\n", *argv);

		return 0;
	}
