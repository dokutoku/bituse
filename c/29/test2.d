/**
 * 第29回
 *
 * See_Also:
 *      https://bituse.info/c/29
 */
module bituse_info.c.sample_29.test2;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

//D言語ではこの宣言は必要ない
//void func(int*, int);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//変数テストのアドレスを代入。
		int test;
		int* point = &test;

		//ポインタを渡すだけ。
		.func(point, 10);

		core.stdc.stdio.printf("%d\n", test);

		return 0;
	}

extern (C)
pure nothrow @trusted @nogc @live
void func(int* temp, int figure)

	do
	{
		*temp = figure * 5;
	}
