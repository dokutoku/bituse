/**
 * 第29回
 *
 * See_Also:
 *      https://bituse.info/c/29
 */
module bituse_info.c.sample_29.test1;


import core.stdc.locale;
import core.stdc.stdio;

//D言語ではこの宣言は必要ない
//void func(int*, int);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//ポインタを渡すだけ。
		int test;
		.func(&test, 10);

		core.stdc.stdio.printf("%d\n", test);

		return 0;
	}

extern (C)
pure nothrow @trusted @nogc @live
void func(int* temp, int figure)

	in
	{
		assert(temp != null);
	}

	do
	{
		*temp = figure * 5;
	}
