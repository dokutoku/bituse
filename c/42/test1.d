/**
 * 第42回
 *
 * See_Also:
 *      https://bituse.info/c/42
 */
module bituse_info.c.sample_42.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.printf("西暦 = ");
		int year;
		core.stdc.stdio.scanf("%d", &year);

		if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
			core.stdc.stdio.printf("%d年は閏年です\n", year);
		} else {
			core.stdc.stdio.printf("%d年は閏年ではありません\n", year);
		}

		return 0;
	}
