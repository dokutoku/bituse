/**
 * 第23回
 *
 * See_Also:
 *      https://bituse.info/c/23
 */
module bituse_info.c.sample_23.test1;


import core.stdc.locale;
import core.stdc.stdio;

alias cp = char*;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] buf = "abcde\0";

		//cpはさっき定義したchar*のこと
		.cp p = &(buf[0]);

		core.stdc.stdio.printf("%s\n", p);

		return 0;
	}
