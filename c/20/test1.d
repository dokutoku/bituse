/**
 * 第20回
 *
 * See_Also:
 *      https://bituse.info/c/20
 */
module bituse_info.c.sample_20.test1;


import core.stdc.locale;
import core.stdc.stdio;

enum TEISU = 50;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int figure = .TEISU / 10;
		core.stdc.stdio.printf("%d\n", figure);

		return 0;
	}
