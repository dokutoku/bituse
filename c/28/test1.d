/**
 * 第28回
 *
 * See_Also:
 *      https://bituse.info/c/28
 */
module bituse_info.c.sample_28.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] buf1 = "あいうえお\0";
		char[50] buf2 = "かきくけこ\0";

		//buf1のポインタを代入
		char* figure1 = &(buf1[0]);

		//ポインタfigure1のポインタをfigure2に代入
		char** figure2 = &figure1;

		//ポインタのポインタfigure2にbuf2の先頭アドレスを格納
		*figure2 = &(buf2[0]);

		//表示
		core.stdc.stdio.printf("%s\n", figure1);

		return 0;
	}
