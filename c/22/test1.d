/**
 * 第22回
 *
 * See_Also:
 *      https://bituse.info/c/22
 */
module bituse_info.c.sample_22.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//4バイト×4のメモリを確保
		int* point = cast(int*)(core.stdc.stdlib.malloc(4 * int.sizeof));

		if (point == null) {
			//メモリ確保エラー
			return 1;
		}

		//スコープを抜けるときに
		scope (exit) {
			assert(point != null);

			//メモリを解放
			core.stdc.stdlib.free(point);
			point = null;
		}

		//代入
		point[0] = 0;
		point[1] = 1;
		point[2] = 2;
		point[3] = 3;

		//表示
		for (size_t i = 0; i < 4; i++) {
			core.stdc.stdio.printf("%d\n", point[i]);
		}

		return 0;
	}
