/**
 * 第27回
 *
 * See_Also:
 *      https://bituse.info/c/27
 */
module bituse_info.c.sample_27.test1;


import core.stdc.locale;
import core.stdc.stdio;

//この変数には他ファイルからはアクセスできない。
private int global1;

//D言語ではこの宣言は必要ない
//int func(int);

//BetterCだとクラッシュする
version (Windows) {
	version (D_BetterC) {
		static assert(false);
	}
}

//extern (C)
//nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int result = .func(5);
		core.stdc.stdio.printf("%d\n", result);

		result = .func(10);
		core.stdc.stdio.printf("%d\n", result);

		return 0;
	}

extern (C)
nothrow @safe @nogc @live
int func(int tmp)

	do
	{
		//static指定子をつけた変数
		static int global2;

		global2 += tmp;

		return global2;
	}
