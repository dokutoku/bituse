/**
 * 第19回
 *
 * See_Also:
 *      https://bituse.info/c/19
 */
module bituse_info.c.sample_19.test1;


import core.stdc.locale;
import core.stdc.stdio;

//D言語でグローバル変数を全スレッド共有にしたい場合は__gsharedを付与する必要がある。
//C言語ではグローバル変数はデフォルトで__gshared
int test;

//D言語ではこの宣言は必要ない
//void func();

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		.test++;

		core.stdc.stdio.printf("%d\n", .test);

		.func();

		core.stdc.stdio.printf("%d\n", .test);

		return 0;
	}

extern (C)
nothrow @nogc
void func()

	do
	{
		.test++;
	}
