/**
 * 第05回
 *
 * See_Also:
 *      https://bituse.info/c/5
 */
module bituse_info.c.sample_5.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char buf;

		core.stdc.stdio.printf("文字を入力して下さい\n");

		core.stdc.stdio.scanf("%c", &buf);

		core.stdc.stdio.printf("入力した値は→%cです。", buf);

		return 0;
	}
