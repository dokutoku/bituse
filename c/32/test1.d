/**
 * 第32回
 *
 * See_Also:
 *      https://bituse.info/c/32
 */
module bituse_info.c.sample_32.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test1 = 0x0100;
		int test2 = 0x0101;

		//16進数で数値を表示させる場合には0xを付与することを忘れないように気をつける。

		//論理積
		core.stdc.stdio.printf("論理積 test1 & test2 → 0x%04X\n", test1 & test2);

		//論理和
		core.stdc.stdio.printf("論理和 test1 | test2 → 0x%04X\n", test1 | test2);

		//排他的論理和
		core.stdc.stdio.printf("排他的論理和 test1 ^ test2 → 0x%04X\n", test1 ^ test2);

		//否定演算
		core.stdc.stdio.printf("否定演算 ~test1 → 0x%04X\n", ~test1);

		return 0;
	}
