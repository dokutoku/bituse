/**
 * 第38回
 *
 * See_Also:
 *      https://bituse.info/c/38
 */
module bituse_info.c.sample_38.test1;


import core.stdc.locale;
import core.stdc.stdio;

//目的値
enum FIGURE = 7;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//前もってソートしておく
		int[10] test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		static assert(test.length > .FIGURE);

		//配列の要素数を取得
		size_t number = test.length;

		//最小の添字
		size_t min = 0;

		//最大の添字
		size_t max = number - 1;

		//中央の要素番号
		size_t mid;

		//最大値と最小値が一致するまでループ
		while (min <= max) {
			mid = (min + max) / 2;

			//まず一致するか比較
			if (test[mid] == .FIGURE) {
				core.stdc.stdio.printf("見つかりました\n");

				return 0;
			} else if (test[mid] < .FIGURE) {
				//目的値が中央値よりも上なら、最小値を中央値の一つ上に設定
				min = mid + 1;

			} else if (test[mid] > .FIGURE) {
				//目的値が中央値よりも下なら、最大値を中央値の一つ下に設定
				max = mid - 1;
			}
		}

		core.stdc.stdio.printf("見つかりませんでした\n");

		return 0;
	}
