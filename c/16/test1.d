/**
 * 第16回
 *
 * See_Also:
 *      https://bituse.info/c/16
 */
module bituse_info.c.sample_16.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		size_t count = 0;

		while (count < 5) {
			core.stdc.stdio.printf("%zu\n", count);

			count++;
		}

		return 0;
	}
