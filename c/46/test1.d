/**
 * 第46回
 *
 * See_Also:
 *      https://bituse.info/c/46
 */
module bituse_info.c.sample_46.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//D言語の2次元配列の宣言の順番はC言語と違うことに注意する。
		char[256][4] buf;

		core.stdc.stdio.puts("文字列を4つ入力してください");

		for (size_t i = 0; i < 4; i++) {
			core.stdc.stdio.scanf("%255s", &(buf[i][0]));
		}

		char[256] tmp;

		for (size_t i = 0; i < 4; i++) {
			for (size_t s = i + 1; s < 4; ++s) {
				if (core.stdc.string.strlen(&(buf[i][0])) > core.stdc.string.strlen(&(buf[s][0]))) {
					core.stdc.string.strcpy(&(tmp[0]), &(buf[i][0]));
					core.stdc.string.strcpy(&(buf[i][0]), &(buf[s][0]));
					core.stdc.string.strcpy(&(buf[s][0]), &(tmp[0]));
				}
			}
		}

		for (size_t i = 0; i < 4; i++) {
			core.stdc.stdio.printf("%zu番目に短い文字列は、【%s】です。\n", i + 1, &(buf[i][0]));
		}

		return 0;
	}
