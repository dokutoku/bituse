/**
 * 第34回
 *
 * See_Also:
 *      https://bituse.info/c/34
 */
module bituse_info.c.sample_34.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] test = [3, 10, 4, 5, 6, 1, 8, 7, 9, 2];

		//配列の要素数を取得
		size_t number = test.length;

		//一時的なワーク領域
		int temp = 0;

		for (size_t i = 0; i < number; i++) {
			//後ろから順番にチェックしていく
			for (size_t s = number - 1; s > i; --s) {
				//一つ下の要素と比較
				if (test[s] < test[s - 1]) {
					//一時的に退避
					temp = test[s - 1];

					//交換
					test[s - 1] = test[s];

					//退避してたやつを戻す
					test[s] = temp;
				}
			}
		}

		//ソートされてるか表示
		for (size_t i = 0; i < number; i++) {
			core.stdc.stdio.printf("%d,", test[i]);
		}

		core.stdc.stdio.puts("");

		return 0;
	}
