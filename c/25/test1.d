/**
 * 第25回
 *
 * See_Also:
 *      https://bituse.info/c/25
 */
module bituse_info.c.sample_25.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//文字列を連結
		char[100] buf = "abcde\0";
		char[100] buf2 = "fghijk\0";
		core.stdc.string.strcat(&(buf[0]), &(buf2[0]));

		core.stdc.stdio.printf("%s\n", &(buf[0]));

		//buf2の文字列を装飾し、bufに代入
		core.stdc.stdio.sprintf(&(buf[0]), "文字列【%s】です。", &(buf2[0]));

		core.stdc.stdio.printf("%s\n", &(buf[0]));

		//バッファ内を初期化
		buf[] = '\0';
		buf2[] = '\0';

		//文字列をコピー
		core.stdc.string.strcpy(&(buf[0]), "コピーしました");

		core.stdc.stdio.printf("%s\n", &(buf[0]));

		//文字列を検索。bufにコピーという文字があるか。
		//あればnull以外が返る
		if (core.stdc.string.strstr(&(buf[0]), "コピー") != null) {
			//puts関数で簡単に文字表示
			core.stdc.stdio.puts("検索文字みつかりました");
		} else {
			core.stdc.stdio.puts("検索文字見つかりませんでした。");
		}

		return 0;
	}
