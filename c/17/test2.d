/**
 * 第17回
 *
 * See_Also:
 *      https://bituse.info/c/17
 */
module bituse_info.c.sample_17.test2;


import core.stdc.locale;
import core.stdc.stdio;

//D言語ではこの宣言は必要ない
//int calc(int test);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int number = .calc(10);
		core.stdc.stdio.printf("%d\n", number);

		return 0;
	}

extern (C)
pure nothrow @safe @nogc @live
int calc(int test)

	do
	{
		int temp = test * 5;

		return temp;
	}
