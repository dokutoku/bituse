/**
 * 第08回
 *
 * See_Also:
 *      https://bituse.info/c/8
 */
module bituse_info.c.sample_8.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[10] buf;
		core.stdc.stdio.scanf("%9s", &(buf[0]));

		core.stdc.stdio.printf("入力した値は→%sです。", &(buf[0]));

		return 0;
	}
