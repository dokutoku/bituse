/**
 * 第21回
 *
 * See_Also:
 *      https://bituse.info/c/21
 */
module bituse_info.c.exsample_21.test1;


import bituse_info.c.exsample_21.test2;
import core.stdc.locale;
import core.stdc.stdio;

int all = 4;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int buf = bituse_info.c.exsample_21.test2.calc(2, 4);
		core.stdc.stdio.printf("%d\n", buf);

		return 0;
	}
