/**
 * 第21回
 *
 * See_Also:
 *      https://bituse.info/c/21
 */
module bituse_info.c.exsample_21.test2;


import bituse_info.c.exsample_21.test1;

extern (C)
pure nothrow @safe @nogc @live
int calc(int value1, int value2)

	do
	{
		return value1 + value2 + bituse_info.c.exsample_21.test1.all;
	}
