/**
 * 第30回
 *
 * See_Also:
 *      https://bituse.info/c/30
 */
module bituse_info.c.sample_30.test3;


import core.stdc.locale;
import core.stdc.stdio;

//D言語ではこの宣言は必要ない
//void func(int*);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] figure = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

		func(figure);

		return 0;
	}

extern (C)
nothrow @nogc
void func(int* temp)

	do
	{
		for (size_t i = 0; i < 10; i++) {
			core.stdc.stdio.printf("%d\n", *temp);

			if (i < 9) {
				++temp;
			}
		}
	}
