/**
 * 第30回
 *
 * See_Also:
 *      https://bituse.info/c/30
 */
module bituse_info.c.sample_30.test1;


import core.stdc.locale;
import core.stdc.stdio;

//D言語ではこの宣言は必要ない
//void func(int[10]);

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] figure = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
		.func(figure);

		core.stdc.stdio.printf("figure[0]: %d\n", figure[0]);

		return 0;
	}

/**
 * 配列の値を要素の数だけ表示する。C言語と違い配列は値渡しであることに注意。
 *
 * Params:
 *      temp = 入力配列
 */
extern (C)
nothrow @nogc
void func(int[10] temp)

	do
	{
		for (size_t i = 0; i < temp.length; i++) {
			core.stdc.stdio.printf("%d\n", temp[i]);
		}

		//ここで適当に値を変更しても呼び出し元には反映されない。
		temp[0] = 100;
	}
