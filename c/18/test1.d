/**
 * 第18回
 *
 * See_Also:
 *      https://bituse.info/c/18
 */
module bituse_info.c.sample_18.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.time;

//D言語ではこの宣言は必要ない
//int func();

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int number = .func();
		core.stdc.stdio.printf("サイコロの目は%dです。\n", number);

		return 0;
	}

/**
 * ランダムっぽいサイコロの目を作成する。
 *
 * Returns: 出たサイコロの目
 */
extern (C)
nothrow @nogc
int func()

	do
	{
		//乱数の初期化
		core.stdc.stdlib.srand(core.stdc.time.time(null));

		//サイコロの目をランダムで出力し、返す。
		return (core.stdc.stdlib.rand() % 6) + 1;
	}
