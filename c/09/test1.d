/**
 * 第09回
 *
 * See_Also:
 *      https://bituse.info/c/9
 */
module bituse_info.c.sample_9.test1;


extern (C)
pure nothrow @safe @nogc @live
int main()

	do
	{
		int[10] test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

		return 0;
	}
