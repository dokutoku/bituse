/**
 * 第13回
 *
 * See_Also:
 *      https://bituse.info/c/13
 */
module bituse_info.c.sample_13.test2;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test = 0;

		core.stdc.stdio.printf("%d\n", test++);

		core.stdc.stdio.printf("%d\n", ++test);

		test += 4;

		core.stdc.stdio.printf("%d\n", test);

		test *= 10;

		core.stdc.stdio.printf("%d\n", test);

		return 0;
	}
