/**
 * 第13回
 *
 * See_Also:
 *      https://bituse.info/c/13
 */
module bituse_info.c.sample_13.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test1 = 300;
		int test2 = 150;
		int enpty = test1 + test2;

		core.stdc.stdio.printf("%d\n", enpty);

		enpty = test1 * test2;

		core.stdc.stdio.printf("%d\n", enpty);

		core.stdc.stdio.printf("%d\n", test1 / test2);

		return 0;
	}
