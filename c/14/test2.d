/**
 * 第14回
 *
 * See_Also:
 *      https://bituse.info/c/14
 */
module bituse_info.c.sample_14.test2;


extern (C)
pure nothrow @safe @nogc @live
int main()

	do
	{
		int test = 300;

		switch (test) {
			case 300:
				break;

			case 400:
				break;

			case 500:
				break;

			//D言語では必ずdefaultが必要
			default:
				break;
		}

		return 0;
	}
