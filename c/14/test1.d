/**
 * 第14回
 *
 * See_Also:
 *      https://bituse.info/c/14
 */
module bituse_info.c.sample_14.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int test = 300;

		if (test == 300) {
			core.stdc.stdio.printf("300だよ!\n");
		} else if (test == 200) {
			core.stdc.stdio.printf("200だよ\n");
		} else {
			core.stdc.stdio.printf("200でも300でもないよ!\n");
		}

		return 0;
	}
