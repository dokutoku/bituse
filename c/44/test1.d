/**
 * 第44回
 *
 * See_Also:
 *      https://bituse.info/c/44
 */
module bituse_info.c.sample_44.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		for (size_t i = 1; i <= 9; i++) {
			for (size_t j = 1; j <= 9; j++) {
				core.stdc.stdio.printf(" %2zu", i * j);
			}

			core.stdc.stdio.printf("\n");
		}

		return 0;
	}
