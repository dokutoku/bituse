/**
 * 第24回
 *
 * See_Also:
 *      https://bituse.info/c/24
 */
module bituse_info.c.sample_24.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test.txtのファイルを作成
		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "w");

		//ファイル作成失敗したらnullポインタが返る
		if (fp == null) {
			core.stdc.stdio.printf("test.txtのファイル作成失敗");

			return -1;
		}

		core.stdc.stdio.fprintf(fp, "あいうえお");
		core.stdc.stdio.fclose(fp);

		//読み込み
		fp = core.stdc.stdio.fopen("test.txt", "r");

		//ファイルが無いとnullが返る
		if (fp == null) {
			core.stdc.stdio.printf("test.txtのファイル読み込み失敗");

			return -1;
		}

		//ファイル読み込み
		char[100] buf;
		core.stdc.stdio.fscanf(fp, "%99s", &(buf[0]));

		//表示
		core.stdc.stdio.printf("%s\n", &(buf[0]));

		//ファイルサイズ取得
		//まずファイルの末尾までファイルポインタを移動
		core.stdc.stdio.fseek(fp, 0, core.stdc.stdio.SEEK_END);

		//現在位置を取得。
		core.stdc.stdio.fpos_t size;
		core.stdc.stdio.fgetpos(fp, &size);

		//ファイルの末尾位置=ファイルサイズなのでそのまま表示
		core.stdc.stdio.printf("ファイルサイズは%dです\n", cast(int)(size));

		//閉じる
		core.stdc.stdio.fclose(fp);

		return 0;
	}
