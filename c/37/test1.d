/**
 * 第37回
 *
 * See_Also:
 *      https://bituse.info/c/37
 */
module bituse_info.c.sample_37.test1;


import core.stdc.locale;
import core.stdc.stdio;

//目的値
enum FIGURE = 7;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] test = [3, 10, 4, 5, 6, 1, 8, 7, 9, 2];
		static assert(test.length > .FIGURE);

		//配列の要素数を取得
		size_t number = test.length;

		for (size_t i = 0; i < number; i++) {
			if (.FIGURE == test[i]) {
				core.stdc.stdio.printf("見つかりました\n");

				return 0;
			}
		}

		core.stdc.stdio.printf("見つかりませんでした\n");

		return 0;
	}
