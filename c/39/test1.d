/**
 * 第39回
 *
 * See_Also:
 *      https://bituse.info/c/39
 */
module bituse_info.c.sample_39.test1;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;

//リスト用構造体定義
struct LIST
{
	.LIST* next;
	int value;
}

//BetterCだとクラッシュする
version (Windows) {
	version (D_BetterC) {
		static assert(false);
	}
}

//リストの先頭を定義
.LIST list;

//D言語ではこの宣言は必要ない
//関数のプロトタイプ宣言
//void Add(int);
//void Del(int);
//void Display();
//void Release();

//extern (C)
//nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//次の要素はまだ空なのでnull入れとく
		.list.next = null;
		char answer;
		int figure;

		while (true) {
			core.stdc.stdio.puts("何をしますか?\n0.終了、1.追加、2.削除、3.表示\n");

			while (true) {
				core.stdc.stdio.scanf("%c", &answer);

				//入力バッファに残る\nをクリアする
				core.stdc.stdio.fflush(core.stdc.stdio.stdin);

				if ((answer != '\r') && (answer != '\n') && (answer != '　') && (answer != '\t')) {
					break;
				}
			}

			if (answer == '0') {
				break;
			}

			switch (answer) {
				case '1':
					core.stdc.stdio.puts("追加する値を入力して下さい");
					core.stdc.stdio.scanf("%d", &figure);
					.Add(figure);

					break;

				case '2':
					core.stdc.stdio.puts("削除する値を入力して下さい");
					core.stdc.stdio.scanf("%d", &figure);
					.Del(figure);

					break;

				case '3':
					.Display();

					break;

				default:
					core.stdc.stdio.puts("正しい値を入力して下さい");

					break;
			}

			//入力バッファに残る\nをクリアする
			core.stdc.stdio.fflush(core.stdc.stdio.stdin);
		}

		//解放
		.Release();

		return 0;
	}

extern (C)
nothrow @nogc
void Add(int temp)

	do
	{
		//新しいリストの領域を確保
		.LIST* p = cast(.LIST*)(core.stdc.stdlib.malloc(.LIST.sizeof));

		if (p == null) {
			core.stdc.stdlib.exit(1);
		}

		//値を代入
		p.value = temp;

		//次の要素は末尾と分かるようにnullを入れとく
		p.next = null;

		//末尾直前のポインタ
		//最初は先頭が末尾直前のポインタになる
		.LIST* prev = &.list;

		//現在の末尾のリストのポインタ;
		.LIST* next;

		//末尾のポインタまで移動
		for (next = .list.next; next != null; next = next.next) {
			prev = next;
		}

		//リストを連結する。
		prev.next = p;

		core.stdc.stdio.puts("追加しました");

	}

extern (C)
nothrow @nogc
void Del(int temp)

	do
	{
		//削除要素の直前の要素のポインタ
		//最初は先頭要素の次のリストからチェックしてるので、
		//削除要素の直前の要素は先頭要素になる。
		.LIST* prev = &.list;

		//リストを末尾(nullになる)までループ
		for (.LIST* p = .list.next; p != null; p = p.next) {
			//その値があれば
			if (p.value == temp) {
				//削除要素の前のリストにつなげる
				//その前に次の要素が末尾ならつなげる必要ないのでチェック
				if (p.next != null) {
					//削除直前の要素につなげる
					prev.next = p.next;

					//削除対象要素の解放
					core.stdc.stdlib.free(p);

					return;
				}

				//削除要素が末尾の要素だった場合の処理
				//末尾要素にnullを入れる。
				prev.next = null;

				//削除対象要素の解放
				core.stdc.stdlib.free(p);

				core.stdc.stdio.puts("削除しました");

				return;
			}

			prev = p;
		}

		core.stdc.stdio.puts("該当の値は見つかりませんでした");
	}

extern (C)
nothrow @nogc
void Display()

	do
	{
		if (.list.next == null) {
			core.stdc.stdio.puts("まだ何もありません");

			return;
		}

		size_t i = 0;

		//nullになるまで全部表示
		for (.LIST* p = .list.next; p != null; p = p.next, i++) {
			core.stdc.stdio.printf("LIST.value[%zu]: %d,\n", i, p.value);
		}

		core.stdc.stdio.puts("");
	}

extern (C)
nothrow @nogc
void Release()

	do
	{
		//次のリストのポインタ
		.LIST* next = .list.next;

		//nullになるまでループ
		while (next != null) {
			//削除対象のポインタを保存
			.LIST* del = next;

			//次のリストのポインタを取得しとく
			next = next.next;

			core.stdc.stdlib.free(del);
			del = null;
		}
	}
