/**
 * 第11回
 *
 * See_Also:
 *      https://bituse.info/c/11
 */
module bituse_info.c.sample_11.test1;


import core.stdc.locale;
import core.stdc.stdio;

//構造体の宣言
struct TEST
{
	int figure;
	short value;
	char letter;
}

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//TEST型の構造体をひとつ確保
		//それぞれのメンバに値を代入
		.TEST test =
		{
			figure: 100,
			value: 200,
			letter: 'A',
		};

		core.stdc.stdio.printf("%d\n", test.figure);
		core.stdc.stdio.printf("%d\n", test.value);
		core.stdc.stdio.printf("%c\n", test.letter);

		return 0;
	}
