/**
 * 第11回
 *
 * See_Also:
 *      https://bituse.info/c/11
 */
module bituse_info.c.sample_11.test2;


import core.stdc.locale;
import core.stdc.stdio;

struct TEST
{
	int figure;
	short value;
	char letter;
}

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		.TEST[2] test =
		[
			{
				figure: 100,
				value: 200,
				letter: 'A',
			},
			{
				figure: 500,
				value: 0,
				letter: 'C',
			},
		];

		core.stdc.stdio.printf("%d\n", test[0].figure);
		core.stdc.stdio.printf("%d\n", test[0].value);
		core.stdc.stdio.printf("%c\n", test[0].letter);

		core.stdc.stdio.printf("%d\n", test[1].figure);
		core.stdc.stdio.printf("%d\n", test[1].value);
		core.stdc.stdio.printf("%c\n", test[1].letter);

		return 0;
	}
