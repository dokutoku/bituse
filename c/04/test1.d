/**
 * 第04回
 *
 * See_Also:
 *      https://bituse.info/c/4
 */
module bituse_info.c.sample_4.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.printf("プログラミング入門\n");

		return 0;
	}
