/**
 * 第35回
 *
 * See_Also:
 *      https://bituse.info/c/35
 */
module bituse_info.c.sample_35.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int[10] test = [3, 10, 4, 5, 6, 1, 8, 7, 9, 2];

		//配列の要素数を取得
		size_t number = test.length;

		//要素を順番に比較
		for (size_t i = 0; i < number; i++) {
			//比較要素を最小値として、その添字を保存
			size_t index = i;

			//最小値の添字より一つ上の添字から比較
			for (size_t s = i + 1; s < number; ++s) {
				if (test[s] < test[index]) {
					//もし小さいのがあれば、その添字を保存
					index = s;
				}
			}

			//一番小さかった添字の要素と現在の比較要素とを交換
			int work = test[i];
			test[i] = test[index];
			test[index] = work;
		}

		//並びかえ出来たか確認
		for (size_t i = 0; i < number; i++) {
			core.stdc.stdio.printf("%d,", test[i]);
		}

		//改行
		core.stdc.stdio.puts("");

		return 0;
	}
