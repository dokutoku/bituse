/**
 * 第15回
 *
 * See_Also:
 *      https://bituse.info/c/15
 */
module bituse_info.c.sample_15.test1;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		for (size_t i = 0; i < 10; i++) {
			//ループ内で以下のような初期化なしの変数を宣言すると、毎回値がリセットされることに注意。C言語では初期化なしの変数は全く初期化されないため、このような現象は起こらない。
			//size_t j;

			core.stdc.stdio.printf("%zu\n", i);
		}

		return 0;
	}
