/**
 * tolower関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/24
 */
module bituse_info.c_func.sample_24.tolower;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int c = core.stdc.ctype.tolower('A');

		core.stdc.stdio.printf("Aを小文字に変換→%c\n", c);

		return 0;
	}
