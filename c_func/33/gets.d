/**
 * gets関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/33
 */
module bituse_info.c_func.sample_33.gets;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.puts("文字列を入力してください。");

		char[50] ch;
		core.stdc.stdio.gets(&(ch[0]));

		core.stdc.stdio.printf("入力した文字は→%s\n", &(ch[0]));

		return 0;
	}
