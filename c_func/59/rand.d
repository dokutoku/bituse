/**
 * rand関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/59
 */
module bituse_info.c_func.sample_59.rand;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//1～10までの乱数を10回表示
		for (size_t i = 0; i < 10; i++) {
			core.stdc.stdio.printf("%zu回目の乱数:%d\n", i + 1, (core.stdc.stdlib.rand() % 10) + 1);
		}

		return 0;
	}
