/**
 * isalnum関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/19
 */
module bituse_info.c_func.sample_19.isalnum;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		if (core.stdc.ctype.isalnum('a')) {
			core.stdc.stdio.puts("aは英数字です");
		}

		if (core.stdc.ctype.isalnum('=')) {
			core.stdc.stdio.puts("=は英数字です。");
		}

		return 0;
	}
