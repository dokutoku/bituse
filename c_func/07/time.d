/**
 * time関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/7
 */
module bituse_info.c_func.sample_7.time;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//現在までの経過時間を取得
		core.stdc.time.time_t tt;
		core.stdc.time.time(&tt);

		core.stdc.stdio.printf("現在時刻:%s", core.stdc.time.ctime(&tt));

		return 0;
	}
