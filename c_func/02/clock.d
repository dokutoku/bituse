/**
 * clock関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/2
 */
module bituse_info.c_func.sample_2.clock;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//150万回ループさせる
		for (size_t i = 0; i < 1500000; i++) {
		}

		core.stdc.time.clock_t second = core.stdc.time.clock();

		core.stdc.stdio.printf("プログラムが起動してからの経過時間は「%d秒」です。\n", second);

		return 0;
	}
