/**
 * memcpy関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/56
 */
module bituse_info.c_func.sample_56.memcpy;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[20] buf = "ABCDDEFG\0";
		string buf2 = "123456789\0";

		//3バイトだけコピー
		core.stdc.string.memcpy(&(buf[0]), &(buf2[0]), 3);

		//表示
		core.stdc.stdio.printf("コピー後のbuf文字列→%s\n", &(buf[0]));

		return 0;
	}
