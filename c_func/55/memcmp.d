/**
 * memcmp関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/55
 */
module bituse_info.c_func.sample_55.memcmp;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		string buf = "ABCD5EFGH\0";
		string buf2 = "ABC123456\0";

		if (core.stdc.string.memcmp(&(buf[0]), &(buf2[0]), 3) == 0) {
			core.stdc.stdio.puts("先頭から3バイトは一致しています。");
		} else {
			core.stdc.stdio.puts("一致していません。");
		}

		return 0;
	}
