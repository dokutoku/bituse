/**
 * isdigit関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/21
 */
module bituse_info.c_func.sample_21.isdigit;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		if (core.stdc.ctype.isdigit('a')) {
			core.stdc.stdio.puts("aは数字です");
		}

		if (core.stdc.ctype.isdigit('5')) {
			core.stdc.stdio.puts("5は数字です。");
		}

		return 0;
	}
