/**
 * fseek関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/48
 */
module bituse_info.c_func.sample_48.fseek;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "r");

		if (fp == null) {
			core.stdc.stdio.printf("fp == null\n");

			return 1;
		}

		scope (exit) {
			assert(fp != null);
			core.stdc.stdio.fclose(fp);
			fp = null;
		}

		//ファイルポインタを末尾まで移動
		core.stdc.stdio.fseek(fp, 0, core.stdc.stdio.SEEK_END);

		//ファイルポインタの位置を取得
		core.stdc.stdio.fpos_t ft;
		core.stdc.stdio.fgetpos(fp, &ft);

		core.stdc.stdio.printf("現在のファイルポインタの位置は「%d」です。\n", cast(int)(ft));

		return 0;
	}
