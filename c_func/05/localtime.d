/**
 * localtime関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/5
 */
module bituse_info.c_func.sample_5.localtime;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//現在時刻を取得
		core.stdc.time.time_t tt;
		core.stdc.time.time(&tt);

		//tm構造体に変換
		core.stdc.time.tm* jikan = core.stdc.time.localtime(&tt);

		//表示
		core.stdc.stdio.printf("現在の時刻は%d時%d分%d秒です。\n", jikan.tm_hour, jikan.tm_min, jikan.tm_sec);

		return 0;
	}
