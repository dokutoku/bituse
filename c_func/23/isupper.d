/**
 * isupper関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/23
 */
module bituse_info.c_func.sample_23.isupper;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		if (core.stdc.ctype.isupper('a')) {
			core.stdc.stdio.puts("aは大文字です");
		}

		if (core.stdc.ctype.isupper('A')) {
			core.stdc.stdio.puts("Aは大文字です。");
		}

		return 0;
	}
