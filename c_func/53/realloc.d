/**
 * realloc関数とpureReallocの使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/53
 *      https://dlang.org/phobos/core_memory.html#.pureRealloc
 */
module bituse_info.c_func.sample_53.realloc;


import core.memory;
import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

/*
 * core.memory.pureReallocを使う場合
 */
version (all)
extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//確保
		char* ch = cast(char*)(core.memory.pureCalloc(1, 100));

		if (ch == null) {
			core.stdc.stdio.printf("ch == null\n");

			return 1;
		}

		//文字を格納。
		core.stdc.stdio.sprintf(ch, "ABCDE");

		//メモリに格納した文字列とそのアドレスを表示
		core.stdc.stdio.printf("文字列「%s」、アドレス「%p」\n", ch, ch);

		//割り当てなおす
		char* ch2 = cast(char*)(core.memory.pureRealloc(ch, 300));

		if (ch2 == null) {
			core.stdc.stdio.printf("ch2 == null\n");

			//reallocのメモリ確保に失敗したら元のメモリをfreeしなければならないことに注意する。
			core.memory.pureFree(ch);
			ch = null;

			return 1;
		} else {
			//reallocのメモリ確保に成功した場合、前のメモリは既にfreeされている。
			ch = null;
		}

		scope (exit) {
			assert(ch2 != null);

			//新しく確保したメモリ解放
			core.memory.pureFree(ch2);
			ch2 = null;
		}

		//これは正しくない。前のポインタを開放しなければならないのはreallocでのメモリ確保に失敗したとき。
		version (none) {
			//もし確保し直したメモリが違うアドレスだったら前のメモリブロックを解放
			if (ch2 != ch) {
				//解放前に新アドレスにコピーしとく
				core.stdc.string.memcpy(ch2, ch, 100);

				core.memory.pureFree(ch);
			}
		}

		//新しく割り当てたメモリの文字列とアドレスを表示
		core.stdc.stdio.printf("文字列「%s」、アドレス「%p」\n", ch2, ch2);

		return 0;
	}

/*
 * core.stdc.stdlib.reallocを使う場合
 */
version (none)
extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//確保
		char* ch = cast(char*)(core.stdc.stdlib.calloc(1, 100));

		if (ch == null) {
			core.stdc.stdio.printf("ch == null\n");

			return 1;
		}

		//文字を格納。
		core.stdc.stdio.sprintf(ch, "ABCDE");

		//メモリに格納した文字列とそのアドレスを表示
		core.stdc.stdio.printf("文字列「%s」、アドレス「%p」\n", ch, ch);

		//割り当てなおす
		char* ch2 = cast(char*)(core.stdc.stdlib.realloc(ch, 300));

		if (ch2 == null) {
			core.stdc.stdio.printf("ch2 == null\n");

			//reallocのメモリ確保に失敗したら元のメモリをfreeしなければならないことに注意する。
			core.stdc.stdlib.free(ch);
			ch = null;

			return 1;
		} else {
			//reallocのメモリ確保に成功した場合、前のメモリは既にfreeされている。
			ch = null;
		}

		scope (exit) {
			assert(ch2 != null);

			//新しく確保したメモリ解放
			core.stdc.stdlib.free(ch2);
			ch2 = null;
		}

		//これは正しくない。前のポインタを開放しなければならないのはreallocでのメモリ確保に失敗したとき。
		version (none) {
			//もし確保し直したメモリが違うアドレスだったら前のメモリブロックを解放
			if (ch2 != ch) {
				//解放前に新アドレスにコピーしとく
				core.stdc.string.memcpy(ch2, ch, 100);

				core.stdc.stdlib.free(ch);
			}
		}

		//新しく割り当てたメモリの文字列とアドレスを表示
		core.stdc.stdio.printf("文字列「%s」、アドレス「%p」\n", ch2, ch2);

		return 0;
	}
