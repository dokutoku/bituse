/**
 * strncat関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/12
 */
module bituse_info.c_func.sample_12.strncat;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test1にtest2の文字列を3文字分だけ連結
		char[20] test1 = "abcde\0";
		char[20] test2 = "12345\0";
		core.stdc.string.strncat(&(test1[0]), &(test2[0]), 3);

		//表示
		core.stdc.stdio.printf("連結した文字列は「%s」です。\n", &(test1[0]));

		return 0;
	}
