/**
 * strcpy関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/10
 */
module bituse_info.c_func.sample_10.strcpy;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test1配列にtest2の文字列をコピー
		char[10] test1 = "abc\0";
		char[10] test2 = "123\0";
		core.stdc.string.strcpy(&(test1[0]), &(test2[0]));

		//表示
		core.stdc.stdio.printf("test1の文字は→%s\n", &(test1[0]));

		return 0;
	}
