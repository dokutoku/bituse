/**
 * puts関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/36
 */
module bituse_info.c_func.sample_36.puts;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//画面に文字列を出力
		core.stdc.stdio.puts("ABCDEFGHIJK");

		return 0;
	}
