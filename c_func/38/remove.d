/**
 * remove関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/38
 */
module bituse_info.c_func.sample_38.remove;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] test;

		core.stdc.stdio.puts("削除するファイル名を50バイト以内で入力してください↓");
		core.stdc.stdio.gets(&(test[0]));

		if (core.stdc.stdio.remove(&(test[0])) == 0) {
			core.stdc.stdio.puts("ファイルの削除に成功");
		} else {
			core.stdc.stdio.puts("ファイルの削除に失敗");
		}

		return 0;
	}
