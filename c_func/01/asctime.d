/**
 * asctime関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/1
 */
module bituse_info.c_func.sample_1.asctime;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//現在時刻取得
		core.stdc.time.time_t tt;
		core.stdc.time.time(&tt);

		//tm構造体に変換
		core.stdc.time.tm* block = core.stdc.time.localtime(&tt);

		//表示
		core.stdc.stdio.printf("%s", asctime(block));

		return 0;
	}
