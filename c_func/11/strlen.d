/**
 * strlen関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/11
 */
module bituse_info.c_func.sample_11.strlen;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[10] test1 = "abcdef\0";
		char[20] test2 = "あいうえお\0";

		//test1の文字列の長さを取得
		size_t len1 = core.stdc.string.strlen(&(test1[0]));

		//test2の文字列の長さを取得
		size_t len2 = core.stdc.string.strlen(&(test2[0]));

		//表示
		core.stdc.stdio.printf("test1の文字列の長さは→%zu\n", len1);
		core.stdc.stdio.printf("test2の文字列の長さは→%zu\n", len2);

		return 0;
	}
