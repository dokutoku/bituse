/**
 * strncpy関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/14
 */
module bituse_info.c_func.sample_14.strncpy;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test1配列にtest2の文字列を3文字コピー
		char[20] test1 = "ab\0";
		char[20] test2 = "12345\0";
		enum size_t copy_length = 3;
		static assert(test1.length > copy_length);
		core.stdc.string.strncpy(&(test1[0]), &(test2[0]), copy_length);

		//test2の文字数より少ない3文字だけのコピーなので\0を末尾に付加する必要がある。
		test1[copy_length] = '\0';

		//表示
		core.stdc.stdio.printf("test1の文字は→%s\n", &(test1[0]));

		//test1配列にtest3の文字列を8文字コピー
		char[20] test3 = "abcde\0";
		static assert(test3.length > 8);
		core.stdc.string.strncpy(&(test1[0]), &(test3[0]), 8);

		//test3の文字数より多い8文字をコピーするので、多い分は\0で埋められるので、
		//手動でヌル文字を付加する必要なし。
		core.stdc.stdio.printf("test1の文字は→%s\n", &(test1[0]));

		return 0;
	}
