/**
 * rename関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/39
 */
module bituse_info.c_func.sample_39.rename;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] oldname;
		core.stdc.stdio.puts("名前を変更するファイル名を50バイト以内で入力してください↓");
		core.stdc.stdio.gets(&(oldname[0]));

		char[50] newname;
		core.stdc.stdio.puts("変更後のファイル名を50バイト以内で入力してください↓");
		core.stdc.stdio.gets(&(newname[0]));

		if (core.stdc.stdio.rename(&(oldname[0]), &(newname[0])) == 0) {
			core.stdc.stdio.puts("ファイル名の変更に成功");
		} else {
			core.stdc.stdio.puts("ファイル名の変更に失敗");
		}

		return 0;
	}
