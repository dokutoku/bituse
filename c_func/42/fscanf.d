/**
 * fscanf関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/42
 */
module bituse_info.c_func.sample_42.fscanf;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("aiueo.txt", "r");

		if (fp == null) {
			core.stdc.stdio.printf("fp == null\n");

			return 1;
		}

		scope (exit) {
			assert(fp != null);
			core.stdc.stdio.fclose(fp);
			fp = null;
		}

		//書式指定で読み込み
		char[50] ch;
		int num;
		core.stdc.stdio.fscanf(fp, "%d%49s", &num, &(ch[0]));

		//文字列表示
		core.stdc.stdio.puts(&(ch[0]));

		//数字表示
		core.stdc.stdio.printf("%d\n", num);

		return 0;
	}
