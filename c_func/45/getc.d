/**
 * getc関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/45
 */
module bituse_info.c_func.sample_45.getc;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "r");

		if (fp == null) {
			core.stdc.stdio.printf("fp == null\n");

			return 1;
		}

		scope (exit) {
			assert(fp != null);
			core.stdc.stdio.fclose(fp);
			fp = null;
		}

		char ch = cast(char)(core.stdc.stdio.getc(fp));

		core.stdc.stdio.printf("読み込んだ文字は→%c\n", ch);

		return 0;
	}
