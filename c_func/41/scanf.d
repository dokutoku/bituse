/**
 * scanf関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/41
 */
module bituse_info.c_func.sample_41.scanf;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[500] ch;

		core.stdc.stdio.puts("文字列を500バイト以内で入力してください");

		core.stdc.stdio.scanf("%499s", &(ch[0]));

		core.stdc.stdio.printf("入力した文字→%s\n", &(ch[0]));

		return 0;
	}
