/**
 * srand関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/60
 */
module bituse_info.c_func.sample_60.srand;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		for (size_t t = 0; t < 5; ++t) {
			//ループのたびにsrandで乱数を初期化
			core.stdc.stdlib.srand(5);
			core.stdc.stdio.printf("%zuループ目の乱数:", t + 1);

			//1～10までの乱数を10回表示
			for (size_t i = 0; i < 10; i++) {
				core.stdc.stdio.printf("%3d ", (core.stdc.stdlib.rand() % 10) + 1);
			}

			//改行
			core.stdc.stdio.puts("");
		}

		core.stdc.stdio.puts("");
		//通常はtime関数の戻り値を引数に使われることが多い。
		core.stdc.stdlib.srand(core.stdc.time.time(null));

		for (size_t t = 0; t < 5; ++t) {
			core.stdc.stdio.printf("%zuループ目の乱数:", t + 1);

			//1～10までの乱数を10回表示
			for (size_t i = 0; i < 10; i++) {
				core.stdc.stdio.printf("%3d ", (core.stdc.stdlib.rand() % 10) + 1);
			}

			//改行
			core.stdc.stdio.puts("");
		}

		return 0;
	}
