/**
 * putchar関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/35
 */
module bituse_info.c_func.sample_35.putchar;


import core.stdc.locale;
import core.stdc.stdio;

//BetterCだとクラッシュする
version (Windows) {
	version (D_BetterC) {
		static assert(false);
	}
}

//extern (C)
//nothrow @nogc
int main()

	do
	{
		//画面に文字を出力
		core.stdc.stdio.putchar('A');

		return 0;
	}
