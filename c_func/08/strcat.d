/**
 * strcat関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/8
 */
module bituse_info.c_func.sample_8.strcat;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//文字列を連結
		char[100] test1 = "abcde\0";
		char[10] test2 = "12345\0";
		core.stdc.string.strcat(&(test1[0]), &(test2[0]));

		//表示
		core.stdc.stdio.printf("連結後の文字列「%s」\n", &(test1[0]));

		return 0;
	}
