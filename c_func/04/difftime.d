/**
 * difftime関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/4
 */
module bituse_info.c_func.sample_4.difftime;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//1つ目の値取得
		core.stdc.time.time_t tt1;
		core.stdc.time.time(&tt1);

		//100万回ループさせる
		for (size_t i = 0; i < 1000000; i++) {
		}

		//二つ目の値取得
		core.stdc.time.time_t tt2;
		core.stdc.time.time(&tt2);

		//差を秒数で表示
		core.stdc.stdio.printf("差は→「%f秒」です。\n", core.stdc.time.difftime(tt1, tt2));

		return 0;
	}
