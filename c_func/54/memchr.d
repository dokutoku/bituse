/**
 * memchr関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/54
 */
module bituse_info.c_func.sample_54.memchr;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		string buf = "ABCD5EFGH\0";

		char* ch = cast(char*)(core.stdc.string.memchr(&(buf[0]), '5', buf.length));

		if (ch != null) {
			core.stdc.stdio.printf("検索文字から表示→%s\n", ch);
		} else {
			core.stdc.stdio.puts("検索文字が見つかりませんでした。");
		}

		return 0;
	}
