/**
 * islower関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/22
 */
module bituse_info.c_func.sample_22.islower;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		if (core.stdc.ctype.islower('a')) {
			core.stdc.stdio.puts("aは小文字です");
		}

		if (core.stdc.ctype.islower('A')) {
			core.stdc.stdio.puts("Aは小文字です。");
		}

		return 0;
	}
