/**
 * putc関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/34
 */
module bituse_info.c_func.sample_34.putc;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		{
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("write.txt", "w");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			//1文字書き込む
			core.stdc.stdio.putc('5', fp);
		}

		int ch;

		{
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("write.txt", "r");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			//読み込んでみる
			ch = core.stdc.stdio.getc(fp);
		}

		core.stdc.stdio.printf("書き込んだ文字は→%c\n", ch);

		return 0;
	}
