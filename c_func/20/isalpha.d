/**
 * isalpha関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/20
 */
module bituse_info.c_func.sample_20.isalpha;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		if (core.stdc.ctype.isalpha('a')) {
			core.stdc.stdio.puts("aは英字です");
		}

		if (core.stdc.ctype.isalpha('5')) {
			core.stdc.stdio.puts("5は英字です。");
		}

		return 0;
	}
