/**
 * toupper関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/25
 */
module bituse_info.c_func.sample_25.toupper;


import core.stdc.ctype;
import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int c = core.stdc.ctype.toupper('a');

		core.stdc.stdio.printf("aを大文字に変換→%c\n", c);

		return 0;
	}
