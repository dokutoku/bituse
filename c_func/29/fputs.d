/**
 * fputs関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/29
 */
module bituse_info.c_func.sample_29.fputs;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		{
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("write.txt", "w");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			//ファイルに書き込み
			core.stdc.stdio.fputs("test123456", fp);
		}

		{
			//書き込まれたか確認するために読み込んでみる。
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("write.txt", "r");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			char[50] buf;
			core.stdc.stdio.fgets(&(buf[0]), buf.length, fp);

			core.stdc.stdio.printf("ファイルに書き込まれた文字列は→%s\n", &(buf[0]));
		}

		return 0;
	}
