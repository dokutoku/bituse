/**
 * memset関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/58
 */
module bituse_info.c_func.sample_58.memset;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[20] buf = "ABCDEFGHIJK\0";

		//先頭から2バイト進めた位置に「1」を3バイト書き込む
		version (all) {
			//D言語版
			buf[2 .. 5] = '1';
		} else {
			core.stdc.string.memset(&(buf[0]) + 2, '1', 3);
		}

		//表示
		core.stdc.stdio.printf("buf文字列→%s\n", &(buf[0]));

		return 0;
	}
