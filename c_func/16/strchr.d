/**
 * strchr関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/16
 */
module bituse_info.c_func.sample_16.strchr;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[20] test = "abcdefghijk\0";
		char* p = core.stdc.string.strchr(&(test[0]), 'g');

		core.stdc.stdio.printf("検索文字が見つかった場所から表示→%s\n", p);

		return 0;
	}
