/**
 * sprintf関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/40
 */
module bituse_info.c_func.sample_40.sprintf;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		string ch = "test\0";
		int num = 100;
		char[100] moji;

		core.stdc.stdio.sprintf(&(moji[0]), "文字「%s」と数字「%d」を組み合わせます", &(ch[0]), num);

		//表示
		core.stdc.stdio.puts(&(moji[0]));

		return 0;
	}
