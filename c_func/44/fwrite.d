/**
 * fwrite関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/44
 */
module bituse_info.c_func.sample_44.fwrite;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] ch = "ABCDEFGH\0";

		{
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "w");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			//1バイトのデータを6個(6バイト分)書き込む。
			core.stdc.stdio.fwrite(&(ch[0]), 1, 6, fp);
		}

		{
			//書き込まれているか確認。
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "r");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			core.stdc.stdio.fgets(&(ch[0]), 50, fp);
		}

		core.stdc.stdio.printf("書き込まれた文字は→%s\n", &(ch[0]));

		return 0;
	}
