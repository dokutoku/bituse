/**
 * strcmp関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/9
 */
module bituse_info.c_func.sample_9.strcmp;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test1とtest2の文字列を比較
		char[10] test1 = "abc\0";
		char[10] test2 = "abc\0";

		if (core.stdc.string.strcmp(&(test1[0]), &(test2[0])) == 0) {
			core.stdc.stdio.puts("test1とtest2の文字列は一致しています。");
		} else {
			core.stdc.stdio.puts("test1とtest2の文字列は一致していません。");
		}

		//test3とtest4の文字列を比較
		char[10] test3 = "abc\0";
		char[10] test4 = "def\0";

		if (core.stdc.string.strcmp(&(test3[0]), &(test4[0])) == 0) {
			core.stdc.stdio.puts("test3とtest4の文字列は一致しています。");
		} else {
			core.stdc.stdio.puts("test3とtest4の文字列は一致していません。");
		}

		return 0;
	}
