/**
 * calloc関数とpureCallocの使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/52
 *      https://dlang.org/phobos/core_memory.html#.pureCalloc
 */
module bituse_info.c_func.sample_52.calloc;


import core.memory;
import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;

/*
 * core.memory.pureCallocを使う場合
 */
version (all)
extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//確保
		char* ch = cast(char*)(core.memory.pureCalloc(1, 100));

		if (ch == null) {
			core.stdc.stdio.printf("ch == null\n");

			return 1;
		}

		scope (exit) {
			assert(ch != null);
			core.memory.pureFree(ch);
			ch = null;
		}

		core.stdc.stdio.puts("文字列を入力してください。");

		core.stdc.stdio.gets(ch);

		core.stdc.stdio.printf("入力した文字は→%s\n", ch);

		return 0;
	}

/*
 * core.stdc.stdlib.callocを使う場合
 */
version (none)
extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//確保
		char* ch = cast(char*)(core.stdc.stdlib.calloc(1, 100));

		if (ch == null) {
			core.stdc.stdio.printf("ch == null\n");

			return 1;
		}

		scope (exit) {
			assert(ch != null);
			core.stdc.stdlib.free(ch);
			ch = null;
		}

		core.stdc.stdio.puts("文字列を入力してください。");

		core.stdc.stdio.gets(ch);

		core.stdc.stdio.printf("入力した文字は→%s\n", ch);

		return 0;
	}
