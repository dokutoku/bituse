/**
 * fgets関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/28
 */
module bituse_info.c_func.sample_28.fgets;


import core.stdc.locale;
import core.stdc.stdio;

//BetterCだとクラッシュする
version (Windows) {
	version (D_BetterC) {
		static assert(false);
	}
}

//extern (C)
//nothrow @nogc
int main()

	do
	{
		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "r");

		if (fp == null) {
			core.stdc.stdio.printf("fp == null\n");

			return 1;
		}

		scope (exit) {
			assert(fp != null);
			core.stdc.stdio.fclose(fp);
			fp = null;
		}

		char[50] buf;
		core.stdc.stdio.fgets(&(buf[0]), 30, fp);

		core.stdc.stdio.puts(&(buf[0]));

		//標準入力からも受け取れる。
		core.stdc.stdio.puts("文字列を入力してください。");
		core.stdc.stdio.fgets(&(buf[0]), 30, core.stdc.stdio.stdin);

		//最後に自動的に改行文字が入る
		core.stdc.stdio.printf("入力した文字列は→%s", &(buf[0]));

		return 0;
	}
