/**
 * fread関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/43
 */
module bituse_info.c_func.sample_43.fread;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("aiueo.txt", "r");

		if (fp == null) {
			core.stdc.stdio.printf("fp == null\n");

			return 1;
		}

		scope (exit) {
			assert(fp != null);
			core.stdc.stdio.fclose(fp);
			fp = null;
		}

		//1バイトを5個読み込み(合計5バイト)
		char[50] ch;
		core.stdc.stdio.fread(&(ch[0]), 1, 5, fp);

		//文字列の末尾にヌル文字(\0)はつけられないので手動で追加。
		ch[5] = '\0';

		//表示
		core.stdc.stdio.printf("読み込んだ文字は→%s\n", &(ch[0]));

		return 0;
	}
