/**
 * mktime関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/6
 */
module bituse_info.c_func.sample_6.mktime;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.time;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//2014年1月29日10時丁度
		core.stdc.time.tm jikan =
		{
			tm_year: 114,
			tm_mon: 0,
			tm_mday: 29,
			tm_hour: 10,
			tm_min: 0,
			tm_sec: 0,
		};

		//time_t構造体に変換
		core.stdc.time.time_t tt = core.stdc.time.mktime(&jikan);

		core.stdc.stdio.printf("2014年1月29日10時をtime_t型で表示→%d\n", tt);

		return 0;
	}
