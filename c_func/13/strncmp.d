/**
 * strncmp関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/13
 */
module bituse_info.c_func.sample_13.strncmp;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		//test1とtest2の文字列を先頭から5文字比較
		char[10] test1 = "abc12\0";
		char[10] test2 = "abcde\0";

		if (core.stdc.string.strncmp(&(test1[0]), &(test2[0]), 5) == 0) {
			core.stdc.stdio.puts("文字列は一致しています。");
		} else {
			core.stdc.stdio.puts("文字列は一致していません。");
		}

		//test1とtest2の文字列を先頭から3文字比較
		if (core.stdc.string.strncmp(&(test1[0]), &(test2[0]), 3) == 0) {
			core.stdc.stdio.puts("文字列は一致しています。");
		} else {
			core.stdc.stdio.puts("文字列は一致していません。");
		}

		return 0;
	}
