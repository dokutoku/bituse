/**
 * fprintf関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/46
 */
module bituse_info.c_func.sample_46.fprintf;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[100] ch = "ABC\0";

		{
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "w");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			int num = 100;
			core.stdc.stdio.fprintf(fp, "ファイルに文字を書き込みます。文字列「%s」、数字「%d」", &(ch[0]), num);
		}

		{
			//書き込めたか調べる
			core.stdc.stdio.FILE* fp = core.stdc.stdio.fopen("test.txt", "r");

			if (fp == null) {
				core.stdc.stdio.printf("fp == null\n");

				return 1;
			}

			scope (exit) {
				assert(fp != null);
				core.stdc.stdio.fclose(fp);
				fp = null;
			}

			core.stdc.stdio.fgets(&(ch[0]), 100, fp);
		}

		core.stdc.stdio.puts(&(ch[0]));

		return 0;
	}
