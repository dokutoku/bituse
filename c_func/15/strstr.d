/**
 * strstr関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/15
 */
module bituse_info.c_func.sample_15.strstr;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[20] test = "abcdefghijk\0";
		char* p = core.stdc.string.strstr(&(test[0]), "fgh");

		core.stdc.stdio.printf("検索文字列が見つかった場所から表示→%s\n", p);

		return 0;
	}
