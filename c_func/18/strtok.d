/**
 * strtok関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/18
 */
module bituse_info.c_func.sample_18.strtok;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[50] test = "12.345.678.9\0";
		char* p = core.stdc.string.strtok(&(test[0]), ".");

		//1つ目のトークン表示
		core.stdc.stdio.puts(p);

		//トークンがnullになるまでループ
		while (p != null) {
			//2回目以降は第一引数はnull
			p = core.stdc.string.strtok(null, ".");

			if (p != null) {
				core.stdc.stdio.puts(p);
			}
		}

		return 0;
	}
