/**
 * printf関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/37
 */
module bituse_info.c_func.sample_37.printf;


import core.stdc.locale;
import core.stdc.stdio;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		int num = 10;

		//10進数で表示
		core.stdc.stdio.printf("10進数で表示→%d\n", num);

		//16進数で表示
		core.stdc.stdio.printf("16進数で表示→%x\n", num);

		//8進数で表示
		core.stdc.stdio.printf("8進数で表示→%o\n", num);

		//浮動小数点を表示
		float ft = 2.215;
		core.stdc.stdio.printf("浮動小数点を表示→%f\n", ft);

		//文字を表示
		char moji = 'c';
		core.stdc.stdio.printf("文字を表示→%c\n", moji);

		//文字列を表示
		string ch = "abcde\0";
		core.stdc.stdio.printf("文字列を表示→%s\n", &(ch[0]));

		//複数の変数を組み合わせて表示
		core.stdc.stdio.printf("複数の変数を組み合わせて表示→%d--%f--%c--%s\n", num, ft, moji, &(ch[0]));

		return 0;
	}
