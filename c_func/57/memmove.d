/**
 * memmove関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/57
 */
module bituse_info.c_func.sample_57.memmove;


import core.stdc.locale;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

extern (C)
nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		char[20] buf = "ABCDDEFG\0";

		//bufの先頭から3バイト進めた位置にbufの先頭から3バイトコピー
		core.stdc.string.memmove(&(buf[0]) + 3, &(buf[0]), 3);

		//表示
		core.stdc.stdio.printf("コピー後のbuf文字列→%s\n", &(buf[0]));

		return 0;
	}
