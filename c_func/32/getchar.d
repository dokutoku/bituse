/**
 * getchar関数の使い方
 *
 * See_Also:
 *      https://bituse.info/c_func/32
 */
module bituse_info.c_func.sample_32.getchar;


import core.stdc.locale;
import core.stdc.stdio;

//BetterCだとクラッシュする
version (Windows) {
	version (D_BetterC) {
		static assert(false);
	}
}

//extern (C)
//nothrow @nogc
int main()

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		core.stdc.stdio.puts("文字を入力してください。");

		int ch = core.stdc.stdio.getchar();

		core.stdc.stdio.printf("入力した文字は→%c\n", ch);

		return 0;
	}
