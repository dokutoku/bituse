/**
 * 第6回 エディットボックスの作成
 *
 * See_Also:
 *      https://bituse.info/winapi/6
 */
module bituse_info.winapi.sample_6.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

//ボタンウィンドウハンドル
core.sys.windows.windef.HWND hwnd_child;

enum CHILD_ID = 1;

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_COMMAND:
				if (core.sys.windows.windef.LOWORD(wp) == .CHILD_ID) {
					switch (core.sys.windows.windef.HIWORD(wp)) {
						case core.sys.windows.winuser.EN_SETFOCUS:
							core.sys.windows.winuser.SetWindowTextW(hwnd, "フォーカス取得");

							return 0;

						case core.sys.windows.winuser.EN_KILLFOCUS:
							core.sys.windows.winuser.SetWindowTextW(hwnd, "フォーカス喪失");

							return 0;

						default:
							break;
					}
				}

				break;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, .hinst, core.sys.windows.windef.NULL);

		//ボタンコントロール作成
		.hwnd_child = core.sys.windows.winuser.CreateWindowW("edit", core.sys.windows.windef.NULL, core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.ES_MULTILINE | core.sys.windows.winuser.ES_LEFT, 20, 20, 300, 200, .hwnd, cast(core.sys.windows.windef.HMENU)(.CHILD_ID), hInstance, core.sys.windows.windef.NULL);

		if ((.hwnd == core.sys.windows.windef.NULL) || (.hwnd_child == core.sys.windows.windef.NULL)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
