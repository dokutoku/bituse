/**
 * 第28回 ディレクトリの移動、作成、削除
 *
 * See_Also:
 *      https://bituse.info/winapi/28
 */
module bituse_info.winapi.sample_28.app;


version (Windows):

private static import core.sys.windows.winbase;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_LBUTTONDOWN:
				wchar[1000] buf;

				//カレントディレクトリを取得
				core.sys.windows.winbase.GetCurrentDirectoryW(buf.length, &(buf[0]));

				core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &(buf[0]), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

				//ディレクトリを作成
				core.sys.windows.winbase.CreateDirectoryW("temp", core.sys.windows.windef.NULL);

				//作ったディレクトリへ移動
				core.sys.windows.winbase.SetCurrentDirectoryW("temp");

				//表示
				core.sys.windows.winbase.GetCurrentDirectoryW(buf.length, &(buf[0]));
				core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &(buf[0]), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

				//削除
				//一旦親フォルダへ移動
				core.sys.windows.winbase.SetCurrentDirectoryW("..");
				core.sys.windows.winbase.RemoveDirectoryW("temp");

				core.sys.windows.winbase.GetCurrentDirectoryW(buf.length, &(buf[0]));
				core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &(buf[0]), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
