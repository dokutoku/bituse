/**
 * 第2回 ウィンドウの作成
 *
 * See_Also:
 *      https://bituse.info/winapi/2
 */
module bituse_info.winapi.sample_2.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 500;

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			//縦横の再描画をする
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,

			//ウィンドウプロシージャの名前
			lpfnWndProc: &.WinProc,

			//使わないから0かNULL
			cbWndExtra: 0,
			cbClsExtra: 0,

			//インスタンスハンドル
			hInstance: hInstance,

			//NULLで問題なし
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,

			//背景を黒に設定
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),

			//クラス登録名は「test」に設定
			lpszClassName: "test",

			//メニューは使用しないのでNULL
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, .hinst, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			return -1;
		}

		return 0;
	}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}
