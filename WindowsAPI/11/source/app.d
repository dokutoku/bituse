/**
 * 第11回 リストビューの作成
 *
 * See_Also:
 *      https://bituse.info/winapi/11
 */
module bituse_info.winapi.sample_11.app;


version (Windows):

private static import core.sys.windows.commctrl;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

//ボタンウィンドウハンドル
core.sys.windows.windef.HWND hwnd_child;

enum
{
	CHILD_ID = 1,
}

//リストビューの列のインデックス
enum
{
	LIST_1,
	LIST_2,
	LIST_3,
}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, .hinst, core.sys.windows.windef.NULL);

		//1グループ目
		.hwnd_child = core.sys.windows.winuser.CreateWindowW("syslistview32", core.sys.windows.windef.NULL, core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CHILD | core.sys.windows.commctrl.LVS_REPORT, 20, 40, 450, 200, .hwnd, cast(core.sys.windows.windef.HMENU)(.CHILD_ID), hInstance, core.sys.windows.windef.NULL);

		core.sys.windows.commctrl.LVCOLUMNW lvcol;
		wchar[64] pszText_buf;

		lvcol.mask = core.sys.windows.commctrl.LVCF_TEXT | core.sys.windows.commctrl.LVCF_SUBITEM | core.sys.windows.commctrl.LVCF_WIDTH | core.sys.windows.commctrl.LVCF_FMT;
		lvcol.fmt = core.sys.windows.commctrl.LVCFMT_LEFT;
		lvcol.cx = 150;
		lvcol.pszText = &(pszText_buf[0]);
		pszText_buf = "一列目\0"w;

		//1列目作成
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_INSERTCOLUMNW, .LIST_1, cast(core.sys.windows.windef.WPARAM)(&lvcol));

		lvcol.cx = 150;
		pszText_buf = "二列目\0"w;

		//2列目作成
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_INSERTCOLUMNW, .LIST_2, cast(core.sys.windows.windef.WPARAM)(&lvcol));

		lvcol.cx = 150;
		pszText_buf = "三列目\0"w;

		//3列目作成
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_INSERTCOLUMNW, .LIST_3, cast(core.sys.windows.windef.WPARAM)(&lvcol));

		//列の作成
		core.sys.windows.commctrl.LVITEMW item;

		item.mask = core.sys.windows.commctrl.LVIF_TEXT;
		item.iItem = 0;
		item.iSubItem = .LIST_1;
		item.pszText = &(pszText_buf[0]);
		pszText_buf = "1列目一行\0"w;

		//一列目はLVM_INSERTITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_INSERTITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		item.iSubItem = .LIST_2;
		pszText_buf = "2列目一行\0"w;

		//二列目以降はLVM_SETITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_SETITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		item.iSubItem = .LIST_3;
		pszText_buf = "3列目一行\0"w;

		//二列目以降はLVM_SETITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_SETITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		//ここからは2行目
		//iItemメンバを1にする。0が一行目を表す。
		item.iItem = 1;
		item.iSubItem = .LIST_1;
		pszText_buf = "1列目2行\0"w;

		//一列目はLVM_INSERTITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_INSERTITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		item.iSubItem = .LIST_2;
		pszText_buf = "2列目2行\0"w;

		//二列目以降はLVM_SETITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_SETITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		item.iSubItem = .LIST_3;
		pszText_buf = "3列目2行\0"w;

		//二列目以降はLVM_SETITEMAで
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.commctrl.LVM_SETITEMW, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&item));

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
