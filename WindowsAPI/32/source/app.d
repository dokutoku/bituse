/**
 * 第32回 コマンドライン引数の取得
 *
 * See_Also:
 *      https://bituse.info/winapi/32
 */
module bituse_info.winapi.sample_32.app;


version (Windows):

private static import core.sys.windows.winbase;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, core.sys.windows.winbase.GetCommandLineW(), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

		return 0;
	}
