/**
 * 第1回 WINAPIとは
 *
 * See_Also:
 *      https://bituse.info/winapi/1
 */
module bituse_info.winapi.sample_1.app;


version (Windows):

import core.sys.windows.windows;

extern (Windows)
pure nothrow @safe @nogc @live
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)

	do
	{
		return 0;
	}
