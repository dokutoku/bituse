/**
 * 第10回 リストボックスの作成
 *
 * See_Also:
 *      https://bituse.info/winapi/10
 */
module bituse_info.winapi.sample_10.app;


version (Windows):

private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

//ボタンウィンドウハンドル
__gshared core.sys.windows.windef.HWND hwnd_child;

enum
{
	CHILD_ID = 1,
}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		core.sys.windows.windef.LRESULT index;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_COMMAND:
				//選択したボックスをウィンドウタイトルに設定
				if (core.sys.windows.windef.LOWORD(wp) == .CHILD_ID) {
					wchar[50] buf = '\0';
					index = core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.winuser.LB_GETCURSEL, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(core.sys.windows.windef.NULL));
					core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.winuser.LB_GETTEXT, index, cast(core.sys.windows.windef.WPARAM)(&(buf[0])));
					core.sys.windows.winuser.SetWindowTextW(hwnd, &(buf[0]));
				}

				break;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, .hinst, core.sys.windows.windef.NULL);

		.hwnd_child = core.sys.windows.winuser.CreateWindowW("listbox", core.sys.windows.windef.NULL, core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.LBS_SORT | core.sys.windows.winuser.LBS_NOTIFY | core.sys.windows.winuser.WS_VSCROLL, 20, 20, 200, 50, .hwnd, cast(core.sys.windows.windef.HMENU)(.CHILD_ID), hInstance, core.sys.windows.windef.NULL);

		//リストボックス追加
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.winuser.LB_ADDSTRING, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&("リスト1\0"w[0])));
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.winuser.LB_ADDSTRING, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&("リスト2\0"w[0])));
		core.sys.windows.winuser.SendMessageW(.hwnd_child, core.sys.windows.winuser.LB_ADDSTRING, cast(core.sys.windows.windef.LPARAM)(core.sys.windows.windef.NULL), cast(core.sys.windows.windef.WPARAM)(&("リスト3\0"w[0])));

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
