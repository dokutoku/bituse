/**
 * 第36回 動画の再生(MCIウィンドウ)
 *
 * See_Also:
 *      https://bituse.info/winapi/36
 */
module bituse_info.winapi.sample_36.app;


version (Windows):

private static import core.sys.windows.vfw;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
__gshared core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 1280;
enum HEIGHT = 720;

enum MOVIE_FILE = "test.wmv";

/*
 * D言語のvfwモジュールの関数にはnothrowや@nogcが設定されていないので、自分で関数宣言する
 */
version (all) {
	nothrow @nogc
	void MCIWndDestroy(core.sys.windows.windef.HWND hwnd)

		do
		{
			core.sys.windows.winuser.SendMessageW(hwnd, core.sys.windows.winuser.WM_CLOSE, 0, 0);
		}

	extern (Windows)
	nothrow @nogc
	core.sys.windows.windef.HWND MCIWndCreateW(core.sys.windows.windef.HWND, core.sys.windows.windef.HINSTANCE, core.sys.windows.windef.DWORD, core.sys.windows.winnt.LPCWSTR);
}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		static core.sys.windows.windef.HWND mcihwnd;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				version (all) {
					.MCIWndDestroy(mcihwnd);
				} else {
					//core.sys.windows.vfw.MCIWndDestroyにはnothrowや@nogcがついていないのでコンパイルエラーになる
					core.sys.windows.vfw.MCIWndDestroy(mcihwnd);
				}

				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_CREATE:
				version (all) {
					mcihwnd = .MCIWndCreateW(hwnd, .hinst, core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.WS_BORDER | core.sys.windows.winuser.WS_VISIBLE, .MOVIE_FILE);
				} else {
					//core.sys.windows.vfw.MCIWndCreateWにはnothrowや@nogcがついていないのでコンパイルエラーになる
					mcihwnd = core.sys.windows.vfw.MCIWndCreateW(hwnd, .hinst, core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.WS_BORDER | core.sys.windows.winuser.WS_VISIBLE, .MOVIE_FILE);
				}

				version (none) {
					core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "右クリック→コマンド→「play」と入力すると再生できます", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
				}

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, 1000, 700, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
