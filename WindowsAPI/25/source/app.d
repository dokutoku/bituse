/**
 * 第25回 ビットマップ画像の表示
 *
 * See_Also:
 *      https://bituse.info/winapi/25
 */
module bituse_info.winapi.sample_25.app;


version (Windows):

private static import bituse_info.winapi.sample_15.define;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
__gshared core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

//リソーススクリプトに記述したID
//enum BMP_ID = "FILENAME";

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		//ビットマップハンドル
		static core.sys.windows.windef.HBITMAP hb;

		//ビットマップ構造体
		static core.sys.windows.wingdi.BITMAP bp;

		//縦、横
		static int width;
		static int height;

		//メモリデバイスコンテキスト
		static core.sys.windows.windef.HDC mhdc;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				//ビットマップハンドルを削除
				core.sys.windows.wingdi.DeleteObject(hb);

				//メモリデバイスコンテキストを破棄
				core.sys.windows.wingdi.DeleteDC(mhdc);

				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_PAINT:
				core.sys.windows.winuser.PAINTSTRUCT ps;
				core.sys.windows.windef.HDC hdc = core.sys.windows.winuser.BeginPaint(hwnd, &ps);

				scope (exit) {
					core.sys.windows.winuser.EndPaint(hwnd, &ps);
				}

				//ファイル読み込み
				version (all) {
					//MAKEINTRESOURCEWを使わない場合
					hb = core.sys.windows.winuser.LoadBitmapW(.hinst, "FILENAME");

					if (hb == core.sys.windows.windef.NULL) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "FILENAME BMP読み込み失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

						return 0;
					}
				} else {
					//MAKEINTRESOURCEWを使う場合
					hb = core.sys.windows.winuser.LoadBitmapW(.hinst, core.sys.windows.winuser.MAKEINTRESOURCEW(bituse_info.winapi.sample_15.define.FILENAME));

					if (hb == core.sys.windows.windef.NULL) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "MAKEINTRESOURCEW BMP読み込み失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

						return 0;
					}
				}

				//画像サイズ取得
				core.sys.windows.wingdi.GetObjectW(hb, core.sys.windows.wingdi.BITMAP.sizeof, &bp);
				width = bp.bmWidth;
				height = bp.bmHeight;

				//メモリデバイスコンテキストを作成
				mhdc = core.sys.windows.wingdi.CreateCompatibleDC(core.sys.windows.windef.NULL);

				//メモリデバイスコンテキストにビットマップを設定
				core.sys.windows.wingdi.SelectObject(mhdc, hb);

				//描画
				core.sys.windows.wingdi.BitBlt(hdc, 0, 0, width, height, mhdc, 0, 0, core.sys.windows.wingdi.SRCCOPY);

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
