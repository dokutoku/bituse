/**
 * 第29回 ファイルの作成、削除
 *
 * See_Also:
 *      https://bituse.info/winapi/29
 */
module bituse_info.winapi.sample_29.app;


version (Windows):

private static import core.sys.windows.basetsd;
private static import core.sys.windows.winbase;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		static core.sys.windows.basetsd.HANDLE h;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_LBUTTONDOWN:
				version (all) {
					h = core.sys.windows.winbase.CreateFileW("test.txt", core.sys.windows.winnt.GENERIC_WRITE, 0, core.sys.windows.windef.NULL, core.sys.windows.winbase.CREATE_ALWAYS, core.sys.windows.winnt.FILE_ATTRIBUTE_NORMAL, core.sys.windows.windef.NULL);

					if (h != core.sys.windows.winbase.INVALID_HANDLE_VALUE) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル作成に成功しました", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

						//ハンドルは必要なくなったら閉じる。
						core.sys.windows.winbase.CloseHandle(h);
						h = core.sys.windows.windef.NULL;

						if (core.sys.windows.winbase.DeleteFileW("test.txt")) {
							core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル削除成功", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						} else {
							core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル削除失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						}
					} else {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル作成に失敗しました", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
					}
				} else {
					h = core.sys.windows.winbase.CreateFileW("test.txt", core.sys.windows.winnt.GENERIC_WRITE, 0, core.sys.windows.windef.NULL, core.sys.windows.winbase.CREATE_NEW, core.sys.windows.winnt.FILE_ATTRIBUTE_NORMAL, core.sys.windows.windef.NULL);

					if (h != core.sys.windows.winbase.INVALID_HANDLE_VALUE) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル作成に成功しました", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						//ハンドルは必要なくなったら閉じる。
						core.sys.windows.winbase.CloseHandle(h);
						h = core.sys.windows.windef.NULL;
					} else {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ファイル作成に失敗しました", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
					}
				}

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
