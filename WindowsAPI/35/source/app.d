/**
 * 第35回 MCIコマンドによる音声ファイル再生
 *
 * See_Also:
 *      https://bituse.info/winapi/35
 */
module bituse_info.winapi.sample_35.app;


version (Windows):

private static import core.sys.windows.basetsd;
private static import core.sys.windows.mmsystem;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		static core.sys.windows.mmsystem.MCI_OPEN_PARMSW open;
		static core.sys.windows.mmsystem.MCI_OPEN_PARMSW open2;
		static core.sys.windows.mmsystem.MCI_OPEN_PARMSW open3;
		static core.sys.windows.mmsystem.MCI_PLAY_PARMS play;
		static core.sys.windows.mmsystem.MCI_PLAY_PARMS play2;
		static core.sys.windows.mmsystem.MCI_PLAY_PARMS play3;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.mmsystem.mciSendCommandW(open.wDeviceID, core.sys.windows.mmsystem.MCI_CLOSE, 0, 0);
				core.sys.windows.mmsystem.mciSendCommandW(open2.wDeviceID, core.sys.windows.mmsystem.MCI_CLOSE, 0, 0);
				core.sys.windows.mmsystem.mciSendCommandW(open3.wDeviceID, core.sys.windows.mmsystem.MCI_CLOSE, 0, 0);

				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_CREATE:
				open.lpstrDeviceType = cast(core.sys.windows.winnt.LPCWSTR)(core.sys.windows.mmsystem.MCI_DEVTYPE_WAVEFORM_AUDIO);
				open.lpstrElementName = "test.wav";

				open3.lpstrDeviceType = cast(core.sys.windows.winnt.LPCWSTR)(core.sys.windows.mmsystem.MCI_DEVTYPE_WAVEFORM_AUDIO);
				open3.lpstrElementName = "test2.wav";

				int result = core.sys.windows.mmsystem.mciSendCommandW(0, core.sys.windows.mmsystem.MCI_OPEN, core.sys.windows.mmsystem.MCI_OPEN_TYPE | core.sys.windows.mmsystem.MCI_OPEN_TYPE_ID | core.sys.windows.mmsystem.MCI_OPEN_ELEMENT, cast(core.sys.windows.basetsd.DWORD_PTR)(&open));

				result = core.sys.windows.mmsystem.mciSendCommandW(0, core.sys.windows.mmsystem.MCI_OPEN, core.sys.windows.mmsystem.MCI_OPEN_TYPE | core.sys.windows.mmsystem.MCI_OPEN_TYPE_ID | core.sys.windows.mmsystem.MCI_OPEN_ELEMENT, cast(core.sys.windows.basetsd.DWORD_PTR)(&open3));

				wchar[1000] buf;

				//エラーなら0以外が返る
				if (result) {
					//エラー取得
					core.sys.windows.mmsystem.mciGetErrorStringW(result, &(buf[0]), buf.length);

					core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &(buf[0]), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

					core.sys.windows.winuser.PostQuitMessage(0);

					return -1;
				}

				//mp3再生の場合
				open2.lpstrDeviceType = "MPEGVideo";
				open2.lpstrElementName = "test.mp3";
				result = core.sys.windows.mmsystem.mciSendCommandW(0, core.sys.windows.mmsystem.MCI_OPEN, core.sys.windows.mmsystem.MCI_OPEN_TYPE | core.sys.windows.mmsystem.MCI_OPEN_ELEMENT, cast(core.sys.windows.basetsd.DWORD_PTR)(&open2));

				//エラーなら0以外が返る
				if (result) {
					//エラー取得
					core.sys.windows.mmsystem.mciGetErrorStringW(result, &(buf[0]), buf.length);

					core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &(buf[0]), core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

					core.sys.windows.winuser.PostQuitMessage(0);

					return -1;
				}

				play.dwCallback = cast(core.sys.windows.windef.DWORD)(hwnd);
				play2.dwCallback = cast(core.sys.windows.windef.DWORD)(hwnd);
				play3.dwCallback = cast(core.sys.windows.windef.DWORD)(hwnd);

				return 0;

			case core.sys.windows.winuser.WM_LBUTTONDOWN:
				core.sys.windows.mmsystem.mciSendCommandW(open.wDeviceID, core.sys.windows.mmsystem.MCI_PLAY, core.sys.windows.mmsystem.MCI_NOTIFY, cast(core.sys.windows.basetsd.DWORD_PTR)(&play));

				return 0;

			case core.sys.windows.winuser.WM_KEYDOWN:
				core.sys.windows.mmsystem.mciSendCommandW(open2.wDeviceID, core.sys.windows.mmsystem.MCI_PLAY, core.sys.windows.mmsystem.MCI_NOTIFY, cast(core.sys.windows.basetsd.DWORD_PTR)(&play2));

				return 0;

			case core.sys.windows.winuser.WM_RBUTTONDOWN:
				core.sys.windows.mmsystem.mciSendCommandW(open3.wDeviceID, core.sys.windows.mmsystem.MCI_PLAY, core.sys.windows.mmsystem.MCI_NOTIFY, cast(core.sys.windows.basetsd.DWORD_PTR)(&play3));

				return 0;

			case core.sys.windows.mmsystem.MM_MCINOTIFY:
				if (lp == open.wDeviceID) {
					if (wp == core.sys.windows.mmsystem.MCI_NOTIFY_SUCCESSFUL) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "再生完了", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						//シークバーを先頭に戻す
						core.sys.windows.mmsystem.mciSendCommandW(open.wDeviceID, core.sys.windows.mmsystem.MCI_SEEK, core.sys.windows.mmsystem.MCI_SEEK_TO_START, 0);
					}

					return 0;
				} else if (lp == open2.wDeviceID) {
					if (wp == core.sys.windows.mmsystem.MCI_NOTIFY_SUCCESSFUL) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "再生完了", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						//シークバーを先頭に戻す
						core.sys.windows.mmsystem.mciSendCommandW(open2.wDeviceID, core.sys.windows.mmsystem.MCI_SEEK, core.sys.windows.mmsystem.MCI_SEEK_TO_START, 0);
					}

					return 0;
				} else if (lp == open3.wDeviceID) {
					if (wp == core.sys.windows.mmsystem.MCI_NOTIFY_SUCCESSFUL) {
						core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "再生完了", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);
						//シークバーを先頭に戻す
						core.sys.windows.mmsystem.mciSendCommandW(open3.wDeviceID, core.sys.windows.mmsystem.MCI_SEEK, core.sys.windows.mmsystem.MCI_SEEK_TO_START, 0);
					}

					return 0;
				}

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
