/**
 * 第15回 スレッドの待機・再開
 *
 * See_Also:
 *      https://bituse.info/winapi/15
 */
module bituse_info.winapi.sample_15.app;


version (Windows):

private static import core.stdc.wchar_;
private static import core.sys.windows.basetsd;
private static import core.sys.windows.winbase;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
__gshared core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

__gshared bool flag = true;

enum
{
	CHILD1 = 100,
	CHILD2,
}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.DWORD Thread(core.sys.windows.winnt.LPVOID* data)

	do
	{
		int count = 0;
		wchar[1000] buf;

		while (.flag) {
			//カウントをウィンドウタイトルに表示
			core.stdc.wchar_.swprintf(&(buf[0]), buf.length, &("%s%d\0"w[0]), data, count);
			core.sys.windows.winuser.SetWindowTextW(.hwnd, &(buf[0]));

			++count;

			//1000ミリ秒(1秒)おきにループ
			core.sys.windows.winbase.Sleep(1000);
		}

		core.sys.windows.winbase.ExitThread(0);

		return 0;
	}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		static core.sys.windows.basetsd.HANDLE th;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_CREATE:
				//スレッドを作成
				th = core.sys.windows.winbase.CreateThread(core.sys.windows.windef.NULL, 0, cast(core.sys.windows.winbase.LPTHREAD_START_ROUTINE)(&Thread), cast(core.sys.windows.winnt.LPVOID)(&("カウント数表示:\0"w[0])), 0, core.sys.windows.windef.NULL);

				return 0;

			case core.sys.windows.winuser.WM_CLOSE:
				//フラグをfalseにしてスレッドを終了させる。
				.flag = false;

				//もしスレッドが停止していたら不具合が起こるので再開
				core.sys.windows.winbase.ResumeThread(th);

				while (true) {
					//スレッドが終わったかチェック
					core.sys.windows.windef.DWORD result;
					core.sys.windows.winbase.GetExitCodeThread(th, &result);

					//終わったらハンドルを閉じる。
					if (result != core.sys.windows.winbase.STILL_ACTIVE) {
						//closehandleで閉じる。
						core.sys.windows.winbase.CloseHandle(th);
						th = core.sys.windows.windef.NULL;

						//ループを抜ける。
						break;
					}

					// 短時間に何回もループされるのを防ぐために待機
					core.sys.windows.winbase.Sleep(100);
				}

				//ウィンドウを破棄
				core.sys.windows.winuser.DestroyWindow(hwnd);

				return 0;

			case core.sys.windows.winuser.WM_COMMAND:
				switch (core.sys.windows.windef.LOWORD(wp)) {
					case .CHILD1:
						core.sys.windows.winbase.SuspendThread(th);

						break;

					case .CHILD2:
						core.sys.windows.winbase.ResumeThread(th);

						break;

					default:
						break;
				}

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		core.sys.windows.winuser.CreateWindowW("button", "開始", core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.BS_PUSHBUTTON, 20, 20, 100, 100, .hwnd, cast(core.sys.windows.windef.HMENU)(.CHILD2), .hinst, core.sys.windows.windef.NULL);
		core.sys.windows.winuser.CreateWindowW("button", "停止", core.sys.windows.winuser.WS_CHILD | core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.BS_PUSHBUTTON, 240, 20, 100, 100, .hwnd, cast(core.sys.windows.windef.HMENU)(.CHILD1), .hinst, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
