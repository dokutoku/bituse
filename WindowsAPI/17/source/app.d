/**
 * 第17回 プロセス・スレッド間の排他制御(ミューテックス)
 *
 * See_Also:
 *      https://bituse.info/winapi/17
 */
module bituse_info.winapi.sample_17.app;


version (Windows):

private static import core.stdc.wchar_;
private static import core.sys.windows.basetsd;
private static import core.sys.windows.winbase;
private static import core.sys.windows.windef;
private static import core.sys.windows.wingdi;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//ウィンドウハンドル
__gshared core.sys.windows.windef.HWND hwnd;

//インスタンスハンドル
core.sys.windows.windef.HINSTANCE hinst;

//ウィンドウ縦横幅
enum WIDTH = 500;
enum HEIGHT = 300;

//グローバル変数count
__gshared int count;

extern (Windows)
nothrow @nogc
core.sys.windows.windef.DWORD Thread1(core.sys.windows.winnt.LPVOID* data)

	do
	{
		//対象のオブジェクトを取得
		core.sys.windows.basetsd.HANDLE h = core.sys.windows.winbase.OpenMutexW(core.sys.windows.winbase.MUTEX_ALL_ACCESS, core.sys.windows.windef.FALSE, "MUTEX");

		//シグナル状態になるまで待つ。
		core.sys.windows.winbase.WaitForSingleObject(h, core.sys.windows.winbase.INFINITE);

		wchar[1000] buf;

		while (.count < 1000) {
			//カウントをウィンドウタイトルに表示
			core.stdc.wchar_.swprintf(&(buf[0]), buf.length, &("%d\0"w[0]), .count);
			core.sys.windows.winuser.SetWindowTextW(.hwnd, &(buf[0]));

			++.count;
			core.sys.windows.winbase.Sleep(10);
		}

		//処理が終わったので解放して他のプロセスへ所有権を移す
		core.sys.windows.winbase.ReleaseMutex(h);

		core.sys.windows.winbase.ExitThread(0);

		return 0;
	}

extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WinProc(core.sys.windows.windef.HWND hwnd, core.sys.windows.windef.UINT msg, core.sys.windows.windef.WPARAM wp, core.sys.windows.windef.LPARAM lp)

	do
	{
		static core.sys.windows.basetsd.HANDLE th1;
		static core.sys.windows.basetsd.HANDLE mutexh;

		switch (msg) {
			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winbase.CloseHandle(th1);
				th1 = core.sys.windows.windef.NULL;
				core.sys.windows.winbase.CloseHandle(mutexh);
				mutexh = core.sys.windows.windef.NULL;
				core.sys.windows.winuser.PostQuitMessage(0);

				return 0;

			case core.sys.windows.winuser.WM_CREATE:
				//ミューテックスオブジェクトを作成
				mutexh = core.sys.windows.winbase.CreateMutexW(core.sys.windows.windef.NULL, core.sys.windows.windef.FALSE, "MUTEX");

				//スレッドを2個作成
				th1 = core.sys.windows.winbase.CreateThread(core.sys.windows.windef.NULL, 0, cast(core.sys.windows.winbase.LPTHREAD_START_ROUTINE)(&Thread1), core.sys.windows.windef.NULL, 0, core.sys.windows.windef.NULL);

				return 0;

			default:
				break;
		}

		return core.sys.windows.winuser.DefWindowProcW(hwnd, msg, wp, lp);
	}

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nShowCmd)

	do
	{
		core.sys.windows.winuser.WNDCLASSW wc =
		{
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WinProc,
			cbWndExtra: 0,
			cbClsExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.windef.NULL,
			hCursor: core.sys.windows.windef.NULL,
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.wingdi.GetStockObject(core.sys.windows.wingdi.BLACK_BRUSH)),
			lpszClassName: "test",
			lpszMenuName: core.sys.windows.windef.NULL,
		};

		if (!core.sys.windows.winuser.RegisterClassW(&wc)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "クラスの登録失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		.hwnd = core.sys.windows.winuser.CreateWindowW("test", "テストウィンドウ", core.sys.windows.winuser.WS_VISIBLE | core.sys.windows.winuser.WS_CAPTION | core.sys.windows.winuser.WS_SYSMENU | core.sys.windows.winuser.WS_MINIMIZEBOX, 0, 0, .WIDTH, .HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		if (.hwnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, "ウィンドウ作成失敗", core.sys.windows.windef.NULL, core.sys.windows.winuser.MB_OK);

			return -1;
		}

		//インスタンスハンドル
		.hinst = hInstance;

		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		//クラス解放
		core.sys.windows.winuser.UnregisterClassW("test", .hinst);

		return 0;
	}
