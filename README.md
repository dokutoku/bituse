# bituseのサンプルプログラムのD言語版

[bituse](https://bituse.info)で紹介されているC言語などのサンプルをD言語に移植したものです。

## レッスン一覧
移植したレッスンのプログラムは以下のようになります。

 - C言語の基本
 - C言語関数の基本
 - WindowsAPIの基礎
 - [Dxlib](https://dxlib.xsrv.jp/)の基礎
 - MySQL(独自に書いた入力補助プログラム)

## プログラムの生成と実行

### D言語のコンパイラのダウンロード

[ダウンロードページ](https://dlang.org/download.html)からD言語のコンパイラをダウンロードします。よくわからない人はDMDを選びましょう。

Windowsの場合は[Visual Studio](https://visualstudio.microsoft.com/ja/downloads/)もダウンロード・インストールしておきましょう。

### ファイルが1つのみの場合

プログラムファイルが一つのみの場合は`rdmd`でファイルを対象ファイルを指定するだけです。

```
rdmd test1.d
```

### dub.jsonがある場合

以下のようにしてdub.jsonがあるフォルダに移動してから、`dub run`を実行します。

```
cd WindowsAPI/01
dub run
```

## 既知の問題

### stdinやstdoutやstderrを使うとプログラムがクラッシュする

これはWindows版でかつ、BetterCでコンパイルした場合に発生する[問題](https://issues.dlang.org/show_bug.cgi?id=19933)です。

main関数にextern (C)がついていたら外して、コマンドラインオプションでもBetterCオプションを加えないようにする必要があります。

### サンプルプログラムのDxlibが動かない

#### 関数が発見できない場合

[Dxlibライブラリ](https://dxlib.xsrv.jp/)をプロジェクトの正しい場所に置く必要があります。

それでも一部の関数は[Dxlib-d](https://gitlab.com/dokutoku/dxlib-d)の問題により発見できません。

#### プログラムがクラッシュする

これは[Dxlib-d](https://gitlab.com/dokutoku/dxlib-d)による問題です。このライブラリを修正する必要があります。
