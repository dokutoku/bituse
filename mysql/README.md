# SQLの入力補助ツール
[bituse](https://bituse.info)の[MySQLの項目](https://bituse.info/mysql/)にあるSQLの作成と学習を補助するためのツールです。

**これは簡単なテスト用のプログラムであることに注意してください。**

## ライセンス

プログラムそのものは自分で作ったものなので、[CC0ライセンス](https://creativecommons.org/publicdomain/zero/1.0/deed.ja)で提供します。

## データベースのダウンロードリンク

 - [MySQL](https://dev.mysql.com/downloads/)
 - [Mroonga](https://github.com/mroonga/mroonga/releases)
 - [MariaDB](https://downloads.mariadb.org/)
 - [PostgreSQL](https://www.postgresql.jp/download)
 - [SQLite](https://sqlite.org/download.html)

## データベースサーバーの立ち上げ

### MySQL

1. 適切な場所にmy.iniの設定ファイルを配置する
2. コンソール経由でbinディレクトリに格納されているmysqldを起動

## データベースのドキュメンテーションのリンク

MySQLやPostgreSQLの日本語マニュアルは以下にあります。

 - [MySQL](https://downloads.mysql.com/docs/refman-5.6-ja.a4.pdf)
 - [PostgreSQL](https://www.postgresql.jp/document/current/index.html)

## 設定ファイル

config.jsonを編集することでデータベースサーバーへの接続先を変更することができます。

config.json以外の設定ファイルの例はconfig-example以下に格納されています。

## 依存ライブラリの概要

このプロジェクト内で使用しているライブラリの変更は、ビルド時に以下のような追加オプションを指定することで行えます。

```
dub run --config=database
```

### 既知の問題

 - PostgreSQLへの接続が失敗することがある

### [hunt-database](https://github.com/huntlabs/hunt-database)

MySQLとPostgreSQLを使用することができます。

### [database](https://github.com/shove70/database)

MySQLとPostgreSQLを使用することができます。

### [SQLite](https://sqlite.org)

[SQLite](https://sqlite.org)の関数定義はD言語に標準で実装されていますが、ライブラリそのものは自分で[SQLiteのサイト](https://sqlite.org)から[ダウンロード](https://sqlite.org/download.html)して用意しなければなりません。

このプロジェクトではwrapper/sqlite3フォルダ以下に配置されていて、ビルド時に実行フォルダと同じ場所に配置されます。

ビルドするためには、ビルド処理を[Visual Studio](https://visualstudio.microsoft.com/ja/downloads/)のコマンド プロンプト上でする必要があります。

## 依存ラインブラリのライセンス

### [database](https://github.com/shove70/database)

databaseのラインセンスはMITでラインセンスされています。

### [hunt-database](https://github.com/huntlabs/hunt-database)

hunt-databaseのラインセンスはApache-2.0でラインセンスされています。

### [SQLite](https://sqlite.org)

SQLiteのラインセンスはPublic Domainでライセンスされています。
