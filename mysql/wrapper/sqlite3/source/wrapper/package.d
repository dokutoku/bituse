module wrapper;


private static import core.stdc.stdio;
private static import etc.c.sqlite3;
private static import std.format;
private static import std.json;
private static import std.stdio;
private static import std.string;
private static import std.uni;

private etc.c.sqlite3.sqlite3* sqlite_connection = null;

void start_connection(const ref std.json.JSONValue connection_info)

	do
	{
		string database_type = std.uni.toLower(connection_info[`type`].str());

		switch (database_type) {
			case `sqlite3`:
			case `sqlite`:
				int result = etc.c.sqlite3.sqlite3_open(std.string.toStringz(connection_info[`database_name`].str()), &.sqlite_connection);

				if (result != etc.c.sqlite3.SQLITE_OK) {
					throw new Exception(std.format.format!(`SQLite3 接続エラー: %s`)(std.string.fromStringz(etc.c.sqlite3.sqlite3_errmsg(.sqlite_connection))));
				}

				break;

			default:
				throw new Exception(std.format.format!(`対応していない接続タイプ: %s`)(connection_info[`type`].str()));
		}
	}

void close_connection()

	in
	{
		assert(.sqlite_connection != null);
	}

	do
	{
		int result = etc.c.sqlite3.sqlite3_close(.sqlite_connection);

		if (result != etc.c.sqlite3.SQLITE_OK) {
			throw new Exception(`sqlite3のクローズに失敗`);
		}

		.sqlite_connection = null;
	}

extern (C)
nothrow @nogc
int select(void* data, int argc, char** argv, char** name)

	do
	{
		if (argc > 0) {
			core.stdc.stdio.printf("%s=%s", name[0], argv[0]);
		}

		for (int i = 1; i < argc; i++) {
			core.stdc.stdio.printf(", %s=%s", name[i], argv[i]);
		}

		core.stdc.stdio.printf("\n");

		return 0;
	}

void execute_sql(string sql)

	in
	{
		assert(.sqlite_connection != null);
	}

	do
	{
		char* error_msg = null;
		sql ~= ((sql.length > 0) && (sql[$ - 1] != ';')) ? (`;`) : (``);
		int result = etc.c.sqlite3.sqlite3_exec(.sqlite_connection, std.string.toStringz(sql), &select, null, &error_msg);

		scope (exit) {
			if (error_msg != null) {
				etc.c.sqlite3.sqlite3_free(error_msg);
				error_msg = null;
			}
		}

		if (result != etc.c.sqlite3.SQLITE_OK) {
			throw new Exception(std.format.format!("SQLite3 エラー: %s")(std.string.fromStringz(error_msg)));
		}
	}
