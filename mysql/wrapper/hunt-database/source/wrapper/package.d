module wrapper;


import hunt.database;
private static import std.format;
private static import std.json;
private static import std.stdio;
private static import std.uni;

private enum connection_type
{
	mysql,
	postgresql,
	sqlite,
	unknown,
}

private Database connection;

void start_connection(const ref std.json.JSONValue connection_info)

	do
	{
		string database_type = std.uni.toLower(connection_info[`type`].str());

		switch (database_type) {
			case `postgre`:
			case `postgres`:
				database_type = `postgresql`;

				goto case;

			case `mysql`:
			case `postgresql`:
				.connection = new Database(std.format.format!(`%s://%s:%s@%s:%d/%s?charset=%s`)(database_type, connection_info[`user`].str(), connection_info[`password`].str(), connection_info[`address`].str(), cast(ushort)(connection_info[`port`].integer), database_type, ((database_type == `postgresql`) ? (`utf-8`) : (`utf8mb4`))));

				break;

			default:
				throw new Exception(std.format.format!(`不明な接続タイプ: %s`)(connection_info[`type`].str()));
		}
	}

void close_connection()

	do
	{
		if (.connection is null) {
			throw new Exception(`データベースに接続されていません。`);
		}

		.connection.close();
	}

void execute_sql(string sql)

	do
	{
		if (.connection is null) {
			throw new Exception(`データベースに接続されていません。`);
		}

		if ((sql.length > 0) && (sql[$ - 1] != ';')) {
			sql ~= `;`;
		}

		hunt.database.RowSet rs = .connection.query(sql);

		foreach (hunt.database.Row row; rs) {
			std.stdio.writeln(row);
		}
	}
