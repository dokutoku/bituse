module wrapper;


private static import database.mysql;
private static import database.postgresql;
private static import std.format;
private static import std.json;
private static import std.stdio;
private static import std.uni;

private enum connection_type
{
	mysql,
	postgresql,
	sqlite,
	unknown,
}

private database.mysql.Connection mysql_connection;
private database.postgresql.Connection postgresql_connection;
private connection_type current_connection;

void start_connection(const ref std.json.JSONValue connection_info)

	do
	{
		string database_type = std.uni.toLower(connection_info[`type`].str());

		switch (database_type) {
			case `mysql`:
				.current_connection = .connection_type.mysql;
				.mysql_connection = new database.mysql.Connection(connection_info[`address`].str(), connection_info[`user`].str(), connection_info[`password`].str(), connection_info[`database_name`].str(), cast(ushort)(connection_info[`port`].integer));

				break;

			case `postgre`:
			case `postgres`:
			case `postgresql`:
				.current_connection = .connection_type.postgresql;
				.postgresql_connection = new database.postgresql.Connection(connection_info[`address`].str(), connection_info[`user`].str(), connection_info[`password`].str(), connection_info[`database_name`].str(), cast(ushort)(connection_info[`port`].integer));

				break;

			default:
				throw new Exception(std.format.format!(`不明な接続タイプ: %s`)(connection_info[`type`].str()));
		}
	}

void close_connection()

	do
	{
		switch (.current_connection) {
			case .connection_type.mysql:
				.mysql_connection.close();
				.current_connection = .connection_type.unknown;

				break;

			case .connection_type.postgresql:
				.postgresql_connection.close();
				.current_connection = .connection_type.unknown;

				break;

			default:
				throw new Exception(`不明な接続タイプ`);
		}
	}

void execute_sql(string sql)

	do
	{
		switch (.current_connection) {
			case .connection_type.mysql:
				string[] rows;

				.mysql_connection.execute
				(
					sql,
					(database.mysql.MySQLRow row) {
						rows ~= row.toString();
					}
				);

				foreach (string row; rows) {
					std.stdio.writeln(row);
				}

				break;

			case .connection_type.postgresql:
				string[] rows;

				.postgresql_connection.execute
				(
					sql,
					(database.postgresql.PgSQLRow row) {
						rows ~= row.toString();
					}
				);

				foreach (string row; rows) {
					std.stdio.writeln(row);
				}

				break;

			default:
				throw new Exception(`不明な接続タイプ`);
		}
	}
