/**
 * bituseのMySQLのコマンド補助とプログラムのサンプルのためのプログラム
 *
 * Author: dokutoku
 * License: CC0
 */
module mysql_sample.common.app;


//import database.mysql;
import std.stdio;
private static import core.stdc.locale;
private static import core.sys.windows.wincon;
private static import std.algorithm;
private static import std.ascii;
private static import std.conv;
private static import std.file;
private static import std.format;
private static import std.getopt;
private static import std.json;
private static import std.string;
private static import std.uni;
private static import std.windows.charset;
private static import wrapper;

/*
 * command line option values
 */
private struct option_value
{
	/**
	 * 設定のJSONファイル
	 */
	string config_file = null;
}

/**
 * コマンドラインオプションをパースする
 *
 * Params:
 *      argv = コマンドラインオプション
 *      value = オプションを格納する
 *
 * Throws: std.getoptの例外
 * Returns: getoptの返り値
 */
auto parse_opt(ref string[] argv, ref option_value value)

	do
	{
		return std.getopt.getopt
		(
			argv,
			std.getopt.config.caseSensitive,
			`config|c`, `設定のJSONファイル。デフォルトはconfig.json`, &(value.config_file)
		);
	}

/**
 * stdinの簡単なラッパー
 *
 * Params:
 *      title = 表示する文字列
 *      allow_words = 許可する文字列
 */
private string read_stdin(const string title, immutable string[] allow_words = null)

	do
	{
		string line;

		while (true) {
			write(title);
			line = std.stdio.stdin.readln();

			if (line is null) {
				continue;
			}

			version (Windows) {
				line = std.windows.charset.fromMBSz(std.string.toStringz(line), core.sys.windows.wincon.GetConsoleCP());
			}

			//文字列の前後の空白を除去する
			line = std.string.strip(line);

			if (line.length == 0) {
				continue;
			}

			if (allow_words == null) {
				return line;
			}

			line = std.uni.toUpper(line);

			if (std.algorithm.canFind(allow_words, line)) {
				return line;
			}
		}
	}

/**
 * stdinの空白許可版
 *
 * Params:
 *      title = 表示する文字列
 *      default_value = デフォルトの値
 */
private string default_read_stdin(const string title, immutable string default_value = null)

	do
	{
		string line;

		write(title);
		line = std.stdio.stdin.readln();

		if (line is null) {
			return default_value;
		}

		version (Windows) {
			line = std.windows.charset.fromMBSz(std.string.toStringz(line), core.sys.windows.wincon.GetConsoleCP());
		}

		//文字列の前後の空白を除去する
		line = std.string.strip(line);

		if (line.length == 0) {
			return default_value;
		}

		return line;
	}

private void write_sql(string sql)

	do
	{
		std.stdio.writeln(sql ~ (((sql.length > 0) && (sql[$ - 1] != ';')) ? (`;`) : (``)));
	}

private void execute_sql(string sql)

	do
	{
		.write_sql(sql);
		wrapper.execute_sql(sql);
	}

/**
 * ラッパー経由でMySQLに接続してSQL文を実行する
 */
void mysql_action(const ref std.json.JSONValue connection_info, ref option_value value)

	do
	{
		wrapper.start_connection(connection_info);

		scope (exit) {
			wrapper.close_connection();
		}

		string lession;
		string sql;

		while (true) {
			lession = read_stdin(`レッスン番号かショートカット記号: `);
			lession = std.uni.toUpper(lession);

			switch (lession) {
				case `SHOW`:
					string action = read_stdin(`データベースかテーブルの表示(DATABASES/TABLES): `, [`DATABASES`, `TABLES`]);
					.execute_sql(std.format.format!(`SHOW %s`)(action));

					continue;

				default:
					break;
			}

			switch (lession) {
				case `EXIT`:
				case `EXIT;`:
					writeln(`終了`);

					return;

				case `DATA`:
				case `1`:
					writeln(`データ型の範囲:`);

					writeln(`[数値型]`);
					writeln(`TINYINT: -128~127`);
					writeln(`SMALLINT: -32,768~32,767`);
					writeln(`MEDIUMINT: -8,388,608~8,388,607`);
					writeln(`INT: -2,147,483,648~2,147,483,647`);
					writeln(`BIGINT: -9,223,372,036,854,775,808~9,223,372,036,854,775,807`);

					writeln(`[浮動小数点型]`);
					writeln(`FLOAT: -3.402823466e+38~-1.175494351e-38, 0, 1.175494351e-38~3.402823466e+38`);
					writeln(`DOUBLE: -1.7976931348623157e+308~2.2250738585072014e-308, 0, 2.2250738585072014e-308~1.7976931348623157e+308`);

					writeln(`[日付、時刻型]`);
					writeln(`DATE: 1000-01-01~9999-12-31`);
					writeln(`DATETIME: 1000-01-01 00:00:00~9999-12-31 23:59:59`);
					writeln(`TIMESTAMP: 1970-01-01 00:00:00~2037-12-31 23:59:59`);
					writeln(`TIME: -838:59:59~838:59:59`);
					writeln(`YEAR: 1901~2155`);

					writeln(`[CHAR型とVARCHAR型]`);
					writeln(`CHAR: 0~255文字`);
					writeln(`VARCHAR: 0~65,535バイト`);

					writeln(`[BINARY型とVARBINARY型]`);
					writeln(`BINARY: 0~255バイト`);
					writeln(`VARBINARY: 0~65,535バイト`);

					writeln(`[BLOB型]`);
					writeln(`TINYBLOB: 255バイト`);
					writeln(`BLOB: 65,535バイト`);
					writeln(`MEDIUMBLOB: 16,777,215バイト`);
					writeln(`LONGBLOB: 429,496,729バイト`);

					writeln(`[TEXT型]`);
					writeln(`TINYTEXT: 255バイト`);
					writeln(`TEXT: 65,535バイト`);
					writeln(`MEDIUMTEXT: 16,777,215バイト`);
					writeln(`LONGTEXT: 429,496,729バイト`);

					break;

				case `2`:
					writeln(`データベースの接続: すでに接続されています。`);

					break;

				case `3`:
					writeln(`データベースの作成・削除:`);

					string action = read_stdin(`データベースの追加か削除か(CREATE/DROP): `, [`CREATE`, `DROP`]);
					string database_name = read_stdin(`データベース名: `);

					sql = action ~ ` DATABASE ` ~ database_name;

					.execute_sql(sql);

					break;

				case `SHOW DATABASES`:
				case `4`:
					writeln(`データベース一覧:`);

					sql = `SHOW DATABASES`;
					.execute_sql(sql);

					break;

				case `CREATE TABLE`:
				case `DROP TABLE`:
				case `5`:
					writeln(`テーブルの作成・削除:`);

					string action = read_stdin(`テーブルの追加か削除か(CREATE/DROP): `, [`CREATE`, `DROP`]);
					string table_name = read_stdin(`テーブル名: `);

					switch (action) {
						case `CREATE`:
							sql = std.format.format!(`CREATE TABLE %s(id INT PRIMARY KEY AUTO_INCREMENT, title VARCHAR(32)) CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin`)(table_name);

							break;

						case `DROP`:
							sql = `DROP TABLE ` ~ table_name;

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `6`:
					writeln(`テーブル名変更:`);

					string table_name_from = read_stdin(`変更前のテーブル名: `);
					string table_name_after = read_stdin(`変更後のテーブル名: `);

					sql = std.format.format!(`ALTER TABLE %s RENAME TO %s`)(table_name_from, table_name_after);

					.execute_sql(sql);

					break;

				case `7`:
					writeln(`テーブルのカラムの追加・変更:`);

					string action = read_stdin(`テーブルのカラムの追加か削除か(ADD/DROP): `, [`ADD`, `DROP`]);
					string table_name = read_stdin(`対象のテーブル名: `);

					switch (action) {
						case `ADD`:
							string column_name = read_stdin(`追加するカラム名: `);
							string column_type = read_stdin(`追加するカラムのデータ型: `);
							sql = std.format.format!(`ALTER TABLE %s ADD %s %s`)(table_name, column_name, column_type);

							break;

						case `DROP`:
							string column_name = read_stdin(`削除するカラム名: `);
							sql = std.format.format!(`ALTER TABLE %s DROP %s`)(table_name, column_name);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `8`:
					writeln(`テーブルのカラム名変更:`);

					string table_name = read_stdin(`対象のテーブル名: `);
					string column_name_from = read_stdin(`変更前のカラム名: `);
					string column_name_after = read_stdin(`変更後のカラム名: `);
					string column_type_after = read_stdin(`変更後のカラムの型: `);

					sql = std.format.format!(`ALTER TABLE %s CHANGE %s %s %s`)(table_name, column_name_from, column_name_after, column_type_after);

					.execute_sql(sql);

					break;

				case `9`:
					writeln(`テーブル一覧やテーブル情報の表示:`);

					string action = read_stdin(`テーブルのリスト表示かテーブルの情報か作成したときのコマンドの表示か(LIST/STATUS/CREATE): `, [`LIST`, `STATUS`, `CREATE`]);

					switch (action) {
						case `LIST`:
							sql = `SHOW TABLES`;

							break;

						case `STATUS`:
							string table_name = read_stdin(`対象のテーブル名: `);
							sql = std.format.format!(`SHOW TABLE STATUS LIKE '%s'`)(table_name);

							break;

						case `CREATE`:
							string table_name = read_stdin(`対象のテーブル名: `);
							sql = std.format.format!(`SHOW CREATE TABLE %s`)(table_name);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `10`:
					writeln(`テーブルのカラム情報取得:`);

					string table_name = read_stdin(`対象のテーブル名: `);
					sql = `SHOW COLUMNS FROM ` ~ table_name;

					.execute_sql(sql);

					break;

				case `11`:
					writeln(`テーブルのカラムNULL許可設定:`);
					writeln(`テーブル作成時、カラムにNOT NULLを設定すれば、NULLデータを格納できなくする。`);

					break;

				case `12`:
					writeln(`テーブルのカラムのデフォルト値:`);
					writeln(`テーブル作成時、カラムにデフォルト値を設定することができる。`);

					break;

				case `13`:
					writeln(`テーブルのカラムのAUTO_INCREMENT:`);
					writeln(`テーブル作成時、カラムにAUTO_INCREMENTを設定することで格納した値が自動的に連番になる。ただし、一つのテーブルにつき一つのカラムにしか設定できない。`);

					break;

				case `14`:
					writeln(`テーブルのカラムのPRIMARY_KEY:`);
					writeln(`テーブル作成時、カラムにPRIMARY_KEYを設定するとそのテーブルの主キーになる。`);

					break;

				case `15`:
					writeln(`テーブルのカラムのUNIQUE制約:`);
					writeln(`テーブル作成時、カラムにUNIQUEを設定すると重複データを格納できなくなる。少し遅くなるので設定されないことも多い。`);

					break;

				case `INDEX`:
				case `16`:
					writeln(`インデックスの追加:`);

					string table_name = read_stdin(`対象のテーブル名: `);
					string index_name = read_stdin(`作成するインデックス名: `);
					string column_name = read_stdin(`対象のカラム名: `);
					string action = read_stdin(`CREATE INDEXかALTER TABLEか(CREATE/ALTER): `, [`CREATE`, `ALTER`]);

					switch (action) {
						case `CREATE`:
							sql = std.format.format!(`CREATE INDEX %s ON %s(%s)`)(index_name, table_name, column_name);

							break;

						case `ALTER`:
							sql = std.format.format!(`ALTER TABLE %s ADD INDEX %s(%s)`)(table_name, index_name, column_name);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `17`:
					writeln(`外部キー制約(省略):`);

					break;

				case `ENGINE`:
				case `18`:
					writeln(`ストレージエンジンの表示・変更:`);

					string action = read_stdin(`利用可能なストレージエンジンの表示かテーブルのエンジン変更(SHOW/ALTER): `, [`SHOW`, `ALTER`]);

					switch (action) {
						case `SHOW`:
							sql = `SHOW ENGINES`;

							break;

						case `ALTER`:
							string table_name = read_stdin(`対象のテーブル名: `);
							string engine_name = read_stdin(`変更先のエンジン名: `);
							sql = `ALTER TABLE ` ~ table_name ~ ` ENGINE=` ~ engine_name;

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `19`:
					writeln(`データを引き継いで新テーブルへ移行(外部キーなどは除く):`);

					string from_name = read_stdin(`移行元のテーブル名: `);
					string to_name = read_stdin(`移行先のテーブル名: `);
					sql = std.format.format!(`CREATE TABLE %s LIKE %s`)(to_name, from_name);
					.execute_sql(sql);
					sql = std.format.format!(`INSERT INTO %s SELECT * FROM %s`)(to_name, from_name);
					.execute_sql(sql);

					break;

				case `INSERT`:
				case `UPDATE`:
				case `DELETE`:
				case `20`:
					writeln(`テーブルにデータを追加・更新・削除:`);

					string action = read_stdin(`追加か更新か削除か(INSERT/UPDATE/DELETE)`, [`INSERT`, `UPDATE`, `DELETE`]);
					string table_name = read_stdin(`テーブル名: `);

					switch (action) {
						case `INSERT`:
							string column_name = read_stdin(`カラム名: `);
							string data = read_stdin(`データ: `);
							sql = std.format.format!(`INSERT INTO %s(%s) VALUES(%s)`)(table_name, column_name, data);

							break;

						case `UPDATE`:
							string column_name = read_stdin(`カラム名: `);
							string data = read_stdin(`データ: `);
							string where;

							while (true) {
								string search_column = default_read_stdin(`検索するカラム名(省略可): `, null);

								if (search_column == null) {
									string confirm = read_stdin(`注意! このままだとテーブル内のデータすべてが書き換えられます。続行しますか(YES/NO)`, [`YES`, `NO`]);

									if (confirm != `YES`) {
										continue;
									}

									break;
								}

								string search_value = read_stdin(`検索する値: `);
								where = std.format.format!(` WHERE %s=%s`)(search_column, search_value);

								break;
							}

							sql = std.format.format!(`UPDATE %s SET %s=%s`)(table_name, column_name, data ~ where);

							break;

						case `DELETE`:
							string column_name = read_stdin(`検索するカラム名: `);
							string data = read_stdin(`検索データ: `);
							sql = std.format.format!(`DELETE FROM %s WHERE %s=%s`)(table_name, column_name, data);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `SELECT`:
				case `LIMIT`:
				case `OFFSET`:
				case `21`:
				case `22`:
				case `23`:
				case `26`:
					writeln(`テーブルからデータ取得(レッスン21~23,26):`);

					string table_name = read_stdin(`テーブル名: `);
					string column_name = default_read_stdin(`表示するカラム(デフォルト値: *): `, `*`);
					string search_column = default_read_stdin(`検索するカラム名(省略可): `, null);
					string where;
					string order;

					if (search_column != null) {
						string search_value = read_stdin(`検索する値: `);
						where = std.format.format!(` WHERE %s=%s`)(search_column, search_value);
					}

					string sort_column = default_read_stdin(`ソートするカラム名(省略可): `, null);

					if (sort_column != null) {
						string sort_value = read_stdin(`昇順でソートするか降順でソートするか(ASC/DESC): `, [`ASC`, `DESC`]);
						order = std.format.format!(` ORDER BY %s %s`)(sort_column, sort_value);
					}

					string limit = default_read_stdin(`最大表示数(省略可): `, null);
					string offset = default_read_stdin(`表示オフセット(省略可): `, null);

					sql = std.format.format!(`SELECT %s FROM %s`)(column_name, table_name ~ where ~ order);
					sql ~= (limit != null) ? ( `LIMIT ` ~ limit) : (null);
					sql ~= (offset != null) ? (` OFFSET ` ~ offset) : (null);

					.execute_sql(sql);

					break;

				case `24`:
					writeln(`テーブルからデータ取得(IN句):`);

					string table_name = read_stdin(`テーブル名: `);
					string column_name = default_read_stdin(`表示するカラム(デフォルト値: *): `, `*`);
					string search_column = read_stdin(`検索するカラム名: `);
					string search_value = read_stdin(`検索する値(複数の場合はカンマ区切り): `);
					string sort_column = default_read_stdin(`ソートするカラム名(省略可): `, null);
					string order = null;

					if (sort_column != null) {
						string sort_value = read_stdin(`昇順でソートするか降順でソートするか(ASC/DESC): `, [`ASC`, `DESC`]);
						order = std.format.format!(` ORDER BY %s %s`)(sort_column, sort_value);
					}

					sql = std.format.format!(`SELECT %s FROM %s WHERE %s IN(%s)%s`)(column_name, table_name, search_column, search_value, order);

					.execute_sql(sql);

					break;

				case `LIKE`:
				case `25`:
					writeln(`テーブルからデータ取得(LIKE検索):`);

					string table_name = read_stdin(`テーブル名: `);
					string column_name = default_read_stdin(`表示するカラム(デフォルト値: *): `, `*`);
					string search_column = read_stdin(`検索するカラム名: `);
					string search_value = read_stdin(`検索する文字列: `);
					string sort_column = default_read_stdin(`ソートするカラム名(省略可): `, null);
					string order = null;

					if (sort_column != null) {
						string sort_value = read_stdin(`昇順でソートするか降順でソートするか(ASC/DESC): `, [`ASC`, `DESC`]);
						order = std.format.format!(` ORDER BY %s %s`)(sort_column, sort_value);
					}

					string limit = default_read_stdin(`最大表示数(省略可): `, null);
					string offset = default_read_stdin(`表示オフセット(省略可): `, null);

					sql = std.format.format!(`SELECT %s FROM %s WHERE %s LIKE '%s'%s`)(column_name, table_name, search_column, search_value, order);
					sql ~= (limit != null) ? ( `LIMIT ` ~ limit) : (null);
					sql ~= (offset != null) ? (` OFFSET ` ~ offset) : (null);

					.execute_sql(sql);

					break;

				case `AS`:
				case `27`:
					writeln(`SELECT時、カラムを別名で表示する:`);

					string table_name = read_stdin(`テーブル名: `);
					string column_name1 = read_stdin(`元のカラム名: `);
					string column_name2 = read_stdin(`別名として表示するカラム名: `);

					sql = std.format.format!(`SELECT %s AS %s FROM %s`)(column_name1, column_name2, table_name);

					.execute_sql(sql);

					break;

				case `TRIGGER`:
				case `28`:
					writeln(`トリガーの作成・確認・削除:`);

					//string action = read_stdin(`トリガの作成か確認か削除か(CREATE/SHOW/DROP): `, [`CREATE`, `SHOW`, `DROP`]);
					string action = read_stdin(`トリガの作成か確認か削除か(SHOW/DROP): `, [`SHOW`, `DROP`]);

					switch (action) {
						/*
						//ToDo: デリミター?
						case `CREATE`:
							string table_name = read_stdin(`テーブル名: `);
							string trigger_name = read_stdin(`トリガー名: `);
							string action2 = read_stdin(`処理の前か後か(BEFORE/AFTER): `, [`BEFORE`, `AFTER`]);
							string action3 = read_stdin(`トリガ対象の処理(INSERT/UPDATE/DELETE): `, [`INSERT`, `UPDATE`, `DELETE`]);
							string action4 = read_stdin(`させたい処理: `);
							sql = std.format.format!("CREATE TRIGGER %s %s %s ON %s FOR EACH ROW\nBEGIN\n%s\nEND")(trigger_name, action2, action3, table_name, action4);

							break;
						*/

						case `SHOW`:
							sql = `SHOW TRIGGERS`;

							break;

						case `DROP`:
							string trigger_name = read_stdin(`トリガー名: `);
							sql = std.format.format!(`DROP TRIGGER %s`)(trigger_name);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `29`:
					writeln(`ユーザー作成・削除:`);

					string user_name = read_stdin(`ユーザー名: `);
					string host_name = default_read_stdin(`ホスト名(デフォルト値: localhost): `, `localhost`);
					string action = read_stdin(`ユーザーの作成か確認か削除か(CREATE/DROP): `, [`CREATE`, `DROP`]);

					switch (action) {
						case `CREATE`:
							string password = default_read_stdin(`パスワード(省略可): `, null);

							if (password != null) {
								password = std.format.format!(` IDENTIFIED BY '%s'`)(password);
							}

							sql = std.format.format!(`CREATE USER '%s'@'%s'%s`)(user_name, host_name, password);

							break;

						case `DROP`:
							sql = std.format.format!(`DROP USER '%s'@'%s'`)(user_name, host_name);

							break;

						default:
							assert(false);
					}

					.execute_sql(sql);

					break;

				case `30`:
					writeln(`ユーザー名変更:`);

					string user_name = read_stdin(`変更前のユーザー名: `);
					string host_name = default_read_stdin(`変更前のホスト名(デフォルト値: localhost): `, `localhost`);
					string new_user_name = read_stdin(`変更後のユーザー名: `);
					string new_host_name = default_read_stdin(`変更後のホスト名(デフォルト値: localhost): `, `localhost`);

					sql = std.format.format!(`RENAME USER %s@%s TO %s@%s`)(user_name, host_name, new_user_name, new_host_name);

					.execute_sql(sql);

					break;

				case `PASSWORD`:
				case `31`:
					writeln(`パスワード設定:`);

					string user_name = default_read_stdin(`対象のユーザー名(省略可): `, null);
					string user_info = null;

					if (user_name != null) {
						string host_name = default_read_stdin(`変更前のホスト名(デフォルト値: localhost): `, `localhost`);
						user_info = std.format.format!(` FOR %s@%s`)(user_name, host_name);
					}

					string password = read_stdin(`設定するパスワードの値: `);
					sql = std.format.format!(`SET PASSWORD%s = PASSWORD('%s')`)(user_info, password);

					.execute_sql(sql);

					break;

				case `32`:
					writeln(`権限の設定:`);

					string kengen = read_stdin(`権限(ALL/SELECT/ALTER/CREATE/DELETE/DROP/INSERT): `, [`ALL`, `SELECT`, `ALTER`, `CREATE`, `DELETE`, `DROP`, `INSERT`]);
					string database_name = default_read_stdin(`データベース名(デフォルト値: *): `, `*`);
					string table_name = default_read_stdin(`テーブル名(デフォルト値: *): `, `*`);
					string user_name = read_stdin(`ユーザー名: `);
					string host_name = default_read_stdin(`ホスト名(デフォルト値: localhost): `, `localhost`);
					sql = std.format.format!(`GRANT %s ON %s.%s to %s@%s`)(kengen, database_name, table_name, user_name, host_name);

					.execute_sql(sql);

					break;

				case `33`:
					writeln(`ユーザー権限の確認:`);

					string user_name = read_stdin(`ユーザー名: `);
					string host_name = default_read_stdin(`ホスト名(デフォルト値: localhost): `, `localhost`);
					sql = std.format.format!(`SHOW GRANTS FOR %s@%s`)(user_name, host_name);

					.execute_sql(sql);

					break;

				case `34`:
					writeln(`ユーザー権限の削除:`);

					string kengen = read_stdin(`権限(ALL/SELECT/ALTER/CREATE/DELETE/DROP/INSERT): `, [`ALL`, `SELECT`, `ALTER`, `CREATE`, `DELETE`, `DROP`, `INSERT`]);
					string database_name = default_read_stdin(`データベース名(デフォルト値: *): `, `*`);
					string table_name = default_read_stdin(`テーブル名(デフォルト値: *): `, `*`);
					string user_name = read_stdin(`ユーザー名: `);
					string host_name = default_read_stdin(`ホスト名(デフォルト値: localhost): `, `localhost`);
					sql = std.format.format!(`REVOKE %s ON %s.%s FROM %s@%s`)(kengen, database_name, table_name, user_name, host_name);

					.execute_sql(sql);

					break;

				case `NOW`:
				case `35`:
					writeln(`現在時刻の取得:`);

					string function_ = read_stdin(`実行する関数名(NOW/CURRENT_TIMESTAMP/LOCALTIME/LOCALTIMESTAMP): `, [`NOW`, `CURRENT_TIMESTAMP`, `LOCALTIME`, `LOCALTIMESTAMP`]);

					sql = std.format.format!(`SELECT %s()`)(function_);

					.execute_sql(sql);

					break;

				case `COUNT`:
				case `36`:
					writeln(`COUNT関数:`);

					string column_name = default_read_stdin(`カラム名(デフォルト値: *): `, `*`);
					string table_name = read_stdin(`テーブル名: `);
					sql = std.format.format!(`SELECT COUNT(%s) FROM %s`)(column_name, table_name);

					.execute_sql(sql);

					break;

				default:
					writeln(`自由にSQL入力:`);

					sql = read_stdin(`SQL実行(一行、最後のセミコロンなし): `);

					.execute_sql(sql);

					break;
			}
		}
	}

/**
 * MySQL以外の場合のSQL文を実行する
 */
void other_action(const ref std.json.JSONValue connection_info, ref option_value value)

	do
	{
		wrapper.start_connection(connection_info);

		scope (exit) {
			wrapper.close_connection();
		}

		while (true) {
			string sql = read_stdin(`MySQL以外のSQL実行(一行): `);
			string action = std.uni.toUpper(sql);

			switch (action) {
				case `EXIT`:
				case `EXIT;`:
					writeln(`終了`);

					return;

				default:
					.execute_sql(sql);

					break;
			}
		}
	}

int main(string[] argv)

	do
	{
		core.stdc.locale.setlocale(core.stdc.locale.LC_ALL, ".utf8");

		option_value value;

		try {
			auto helo_info = .parse_opt(argv, value);

			if (helo_info.helpWanted) {
				std.getopt.defaultGetoptPrinter(`ヘルプ:`, helo_info.options);

				return 0;
			}

			string text_file = (value.config_file != null) ? (value.config_file) : (`config.json`);
			string content = std.file.readText(text_file);
			std.json.JSONValue j = std.json.parseJSON(content, std.json.JSONOptions.strictParsing);
			string database_type = std.uni.toLower(j[`type`].str());

			if (database_type != `mysql`) {
				.other_action(j, value);
			} else {
				mysql_action(j, value);
			}
		} catch (Exception e) {
			std.stdio.stderr.writeln(e.msg);

			return 1;
		}

		return 0;
	}
